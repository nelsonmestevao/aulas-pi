#include <stdio.h>

void linha(int n) {
    int i = 0;
    while (i < n) {
        printf("#");
        i = i + 1; // i++
    }
    printf("\n");
}

void quadrado(int n) {
    int i = 0;
    while (i < n) {
        linha(n);
        i++; // i = i + 1;
    }
}

int main() {
    quadrado(5);
    return 0;
}
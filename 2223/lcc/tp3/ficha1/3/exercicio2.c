#include <stdbool.h>
#include <stdio.h>

void xadrez(int n) {
    int i, j;

    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++) {
            if ((i + j) % 2 == 0) {
                putchar('#');
            } else {
                putchar('_');
            }
        }
        putchar('\n');
    }
}

void xadrez2(int n) {
    int c = 0;
    int i, j;

    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++) {
            if (c % 2 == 0) {
                putchar('#');
            } else {
                putchar('_');
            }
            c++;
        }
        putchar('\n');
    }
}

void xadrez3(int n) {
    int c;
    for (c = 1; c <= n * n; c++) {
        if (c % 2 == 0) {
            putchar('_');
        } else {
            putchar('#');
        }

        if (c % n == 0) {
            putchar('\n');
        }
    }
}

void mostraCaracterQuandoComecaComAsterisco(int i) {
    if (i % 2 == 0) {
        putchar('#');
    } else {
        putchar('_');
    }
}

void mostraCaracterQuandoNaoComecaComAsterisco(int i) {
    if (i % 2 == 0) {
        putchar('_');
    } else {
        putchar('#');
    }
}

void linha(int n, bool comecaComAsterisco) {
    int i;
    for (i = 0; i < n; i++)
        if (comecaComAsterisco) {
            mostraCaracterQuandoComecaComAsterisco(i);
        } else {
            mostraCaracterQuandoNaoComecaComAsterisco(i);
        }
    putchar('\n');
}

void xadrez4(int n) {
    int i;
    for (i = 0; i < n; i++) {
        linha(n, i % 2 == 0);
    }
}

int main() {
    xadrez(5);
    printf("---------\n");
    xadrez2(5);
    printf("---------\n");
    xadrez3(5);
    printf("---------\n");
    xadrez4(5);

    return 0;
}
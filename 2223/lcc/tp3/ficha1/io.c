#include <stdio.h>

int main() {
    int d;

    printf("Preciso de um digito: ");
    scanf("%i", &d);

    printf("Recebi o valor %c %i\n", d, d);

    return 0;
}
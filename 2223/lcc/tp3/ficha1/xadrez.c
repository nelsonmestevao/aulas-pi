#include <stdio.h>

void xadrez(int n) {
    int i, j;

    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++) {
            if ((i + j) % 2 == 0) {
                putchar('#');
            } else {
                putchar('_');
            }
        }
        putchar('\n');
    }
}

int main() {
    int n;

    printf("Tamanho do tabuleiro: ");
    scanf("%i", n);

    printf("\nO Tabuleiro:\n");
    xadrez(n);

    return 0;
}
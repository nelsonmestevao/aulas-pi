#include <stdio.h>

// void swap(int *a, int *b) {
//     int tmp = (*a);
//     (*a) = (*b);
//     (*b) = tmp;
// }

void swap(int *a, int *b) {
    int tmp = (*b);
    (*b) = (*a);
    (*a) = tmp;
}

int main() {
    int a = 42;
    int b = 27;

    swap(&a, &b);

    printf("a = %d\n", a);
    printf("b = %d\n", b);

    return 0;
}
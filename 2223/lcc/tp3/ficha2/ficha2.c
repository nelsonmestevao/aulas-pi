#include <assert.h>
#include <stdio.h>

float multInt1(int n, float m) {
    float r = 0;
    int i;

    for (i = 0; i < n; i++) {
        r += m; // r = r + m;
    }

    return r;
}

float multInt2(int n, float m) {
    float r = 0;
    int c = 0;

    while (n > 0) {
        c++;
        if (n % 2 != 0) {
            r += m;
            c++;
        }
        n >>= 1; // n = n / 2;
        m *= 2;  // m = m * 2;
        c += 2;
    }

    printf("Operações feitas: %d\n", c);

    return r;
}

int mdc1(int a, int b) {
    // if (a > b) {
    //     menor = b;
    // } else {
    //     menor = a;
    // }
    int menor = (a > b) ? b : a;
    int i;
    int r;

    for (i = 1; i <= menor; i++) {
        if (a % i == 0 && b % i == 0)
            r = i;
    }

    return r;
}

int mdc1_b(int a, int b) {
    int menor = (a > b) ? b : a;
    int i;

    for (i = menor; i >= 1; i--) {
        if (a % i == 0 && b % i == 0)
            return i;
    }

    return 1;
}

int mdc2(int a, int b) {
    while (a != 0 && b != 0) {
        if (a > b) {
            a %= b;
        } else if (b > a) {
            b %= a;
        } else {
            return a;
        }
    }

    if (a == 0)
        return b;

    return a;
}

int fib1(int n) {
    if (n == 0)
        return n;

    if (n <= 2) {
        return 1;
    }

    return fib1(n - 1) + fib1(n - 2);
}

// int fib1(int n) {
//     int r;

//     if (n < 2) {
//         r = 1;
//     } else {
//         r = fib1(n -1) + fib1(n -2);
//     }

//     return r;
// }

int fib2(int n) {
    if (n == 0)
        return n;

    int acc1, acc2;
    acc1 = acc2 = 1;

    int i;
    for (i = 3; i <= n; i++) {
        acc2 += acc1;
        acc1 = acc2 - acc1;
    }

    return acc2;
}

int main() {
    assert(35109 == multInt1(83, 423.0));
    assert(35109 == multInt2(83, 423.0));

    assert(9 == mdc1(126, 45));
    assert(9 == mdc1_b(126, 45));
    assert(9 == mdc2(126, 45));

    assert(8 == fib1(6));
    assert(8 == fib2(6));

    return 0;
}
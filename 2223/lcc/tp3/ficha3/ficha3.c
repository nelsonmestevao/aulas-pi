
void swapM(int *a, int *b) {
    int tmp = (*b);
    (*b) = (*a);
    (*a) = tmp;
}

void swap(int v[], int i, int j) {
    int tmp = v[i];
    v[j] = v[i];
    v[i] = v[j];
}

void invertArrayM(int v[], int N) {
    int stop = N / 2;
    int i;

    for (i = 0; i < stop; i++) {
        swapM(&v[i], &v[N - i - 1]);
        // swapM(v + sizeof(int) * i, v + sizeof(int) * (N - i - 1));
    }
}

void invertArray(int v[], int N) {
    int stop = N / 2;
    int i;

    for (i = 0; i < stop; i++) {
        swap(v, i, N - i - 1);
    }
}
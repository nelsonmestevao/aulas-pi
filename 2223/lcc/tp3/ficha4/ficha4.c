#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

int exemplo(int i, char x) {
    int r;

    // if (x == 'a') {
    //     r= i * 2;
    // } else if (x == 'b') {
    //     r = i * 4;
    // } else {
    //     r= i + 1;
    // }

    switch (x) {
        case 'a':
            r = i * 2;
            break;
        case 'b':
            r = i * 4;
            break;
        case 'c':
            r = i + 1;
            break;
        default:
            r = 0;
    }

    return r;
}

int eVogal(char c) {
    switch (tolower(c)) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            return 1;
        default:
            return 0;
    }
}

int contaVogais(char *s) {
    int c = 0;
    int pos = 0;

    while (s[pos] != '\0') {
        if (eVogal(s[pos])) {
            c++;
        }
        pos++;
    }

    return c;
}

int retiraVogaisRep1(char *s) {
    int repetidasContador = 0;
    int comprimento = strlen(s);
    char aux[comprimento];
    int pos = 1;
    int posAux = 1;

    aux[0] = s[0];

    while (s[pos] != '\0') {
        if (!eVogal(s[pos]) || s[pos] != s[pos - 1]) {
            aux[posAux] = s[pos];
            posAux++;
        } else {
            repetidasContador++;
        }
        pos++;
    }
    aux[posAux] = '\0';

    int i = 0;
    while (aux[i] != '\0') {
        s[i] = aux[i];
        i++;
    }

    s[i] = '\0';

    return repetidasContador;
}

int retiraVogaisRep2(char *s) {
    char *n = s;
    char *p = n + 1;

    while (*p != '\0') {
        // if (!(eVogal(*n) && eVogal(*p))) {
        if (!(eVogal(*n) && eVogal(*p) && *p == *n)) {
            n++;
            *n = *p;
        }
        p++;
    }

    *(n + 1) = '\0';

    return p - n - 1;
}

int main() {
    assert(contaVogais("PI 2022") == 1);
    assert(contaVogais("PI e C") == 2);

    char s[10] = "A cooubraa";
    int r = retiraVogaisRep2(s);

    assert(r == 2);

    printf("%s\n", s);
}
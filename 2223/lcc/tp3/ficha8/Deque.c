#include "Deque.h"
#include "Listas.h"
#include <stdio.h>
#include <stdlib.h>

void initDeque(Deque *q) {
    // (*q).back = NULL;
    // (*q).front = NULL;

    q->back = NULL;
    q->front = NULL;
}

int DisEmpty(Deque q) {
    if (q.back == NULL)
        return 0;
    else
        return 1;
    // return q.back==NULL;
}
int pushBack(Deque *q, int x) {
    DList c = newDList(x, NULL);
    if (c == NULL) {
        return 1;
    }

    if (DisEmpty(*q)) {
        q->front = c;
        q->back = c;
    } else {
        q->back->prox = c;
        c->ant = q->back;
        c->prox = q->front;
        c->front->ant = c;
        q->back = c;
    }

    return 0;
}

int pushFront(Deque *q, int x) {
    DList c = newDList(x, NULL);
    if (c == NULL) {
        return 1;
    }
    if (DisEmpty(*q)) {
        q->front = c;
        q->back = c;
    } else {
        q->front->ant = c;
        c->prox = q->front;
        c->ant = q->back;
        q->front = c;
    }
    return 0;
}

int popBack(Deque *q, int *x) { return -1; }
int popFront(Deque *q, int *x) { return -1; }
int popMax(Deque *q, int *x) { return -1; }
int back(Deque q, int *x) { return -1; }
int front(Deque q, int *x) { return -1; }

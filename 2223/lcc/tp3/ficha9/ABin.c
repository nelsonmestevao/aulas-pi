#include "ABin.h"

ABin newABin(int r, ABin e, ABin d) {
    ABin a = malloc(sizeof(struct nodo));
    if (a != NULL) {
        a->valor = r;
        a->esq = e;
        a->dir = d;
    }
    return a;
}

ABin RandArvFromArray(int v[], int N) {
    ABin a = NULL;
    int m;
    if (N > 0) {
        m = rand() % N;
        a = newABin(v[m], RandArvFromArray(v, m),
                    RandArvFromArray(v + m + 1, N - m - 1));
    }
    return a;
}

int max(int a, int b) {
    if (a < b) {
        return b;
    }

    return a;
}

int altura(ABin a) {
    int esq, dir, alt = 0;
    if (a != NULL) {
        esq = 1 + altura(a->esq);
        dir = 1 + altura(a->dir);
        alt = max(esq, dir);
    }
    return alt;
}

int eFolha(ABin a) {
    if (a->esq == NULL && a->dir == NULL) {
        return 1;
    }

    return 0;
}

int nFolhas(ABin a) {
    if (a == NULL)
        return 0;

    if (eFolha(a)) {
        return 1;
    }

    return nFolhas(a->esq) + nFolhas(a->dir);
}

ABin maisEsquerda(ABin a) {
    if (a == NULL || a->esq == NULL) {
        return a;
    }

    return maisEsquerda(a->esq);
}

void imprimeNivel(ABin a, int l) {
    if (a != NULL) {
        if (l == 0) {
            printf("-> %d\n", a->valor);
        } else {
            imprimeNivel(a->esq, l - 1);
            imprimeNivel(a->dir, l - 1);
        }
    }
}

int procuraE(ABin a, int x) {
    if (a == NULL)
        return 0;

    if (a->valor == x)
        return 1;

    return procuraE(a->esq, x) || procuraE(a->dir, x);
}

struct nodo *procura(ABin a, int x) { return NULL; }

int nivel(ABin a, int x) { return (-1); }

void imprimeAte(ABin a, int x) {}

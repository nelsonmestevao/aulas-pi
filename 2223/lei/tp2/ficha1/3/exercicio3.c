#include <stdio.h>

void linha(int n) {
    int i;
    for (i = 0; i < n; i++) {
        putchar('#');
    }
    putchar('\n');
}

void quadrado(int n) {
    int i;
    for (i = 0; i < n; i++) {
        linha(n);
    }
}

// i = i + 1;
// i += 1;
// i++;

void xadrez_da_wish(int n) {
    int c; // contador

    for (c = 1; c <= n * n; c++) {
        if (c % 2 == 0) {
            putchar('_');
        } else {
            putchar('#');
        }

        if (c % n == 0) {
            putchar('\n');
        }
    }
}

void xadrez(int n) {
    int i, j;
    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++) {
            if ((i + j) % 2 == 0) {
                putchar('#');
            } else {
                putchar('_');
            }
        }
        putchar('\n');
    }
}

void trianguloH(int n) { printf("TrianguloH %d ainda não está feito...\n", n); }

void trianguloV(int n) { printf("TrianguloV %d ainda não está feito...\n", n); }

int circulo(int raio) {
    printf("Circulo %d ainda não está feito...\n", raio);
    return 0;
}

int main() {
    int n;
    printf("Quadrado de que dimensão? ");
    scanf("%i", &n);

    quadrado(n);
    putchar('\n');
    putchar('\n');
    printf("Xadrez:\n\n");
    xadrez(5);
    putchar('\n');
    putchar('\n');
    xadrez_da_wish(5);
    putchar('\n');
    putchar('\n');
    trianguloH(5);
    putchar('\n');
    putchar('\n');
    trianguloV(5);
    putchar('\n');
    putchar('\n');
    printf("\nForam usados %d caracteres para fazer o circulo\n", circulo(5));
    return 0;
}

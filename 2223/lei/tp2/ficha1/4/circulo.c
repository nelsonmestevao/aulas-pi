#include <math.h>
#include <stdio.h>

// compilar com: gcc -lm circulo.c

void draw_circunference(int radius) {
    int diameter = radius * 2;
    int x, y;

    for (y = 0; y <= diameter; y++) {
        for (x = 0; x <= diameter; x++) {
            int distance = (int)sqrt(pow(x - radius, 2) + pow(y - radius, 2));
            if (distance == radius) {
                printf("#");
            } else {
                printf(" ");
            }
        }
        printf("\n");
    }
}

int draw_circle(int radius) {
    int diameter = radius * 2;
    int x, y;
    int c; // contador de '#'

    for (y = 0; y <= diameter; y++) {
        for (x = 0; x <= diameter; x++) {
            int distance = (int)sqrt(pow(x - radius, 2) + pow(y - radius, 2));
            if (distance <= radius) {
                printf("#");
                c++;
            } else {
                printf(" ");
            }
        }
        printf("\n");
    }

    return c;
}

int main() {
    int c = draw_circle(6);
    putchar('\n');
    putchar('\n');
    printf("Foram precisos  %i '#'\n", c);
    // draw_circunference(4);
}
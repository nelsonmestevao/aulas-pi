#include "abin.h"

ABin newABin(int r, ABin e, ABin d) {
    ABin a = malloc(sizeof(struct nodo));
    if (a != NULL) {
        a->valor = r;
        a->esq = e;
        a->dir = d;
    }
    return a;
}

ABin RandArvFromArray(int v[], int N) {
    ABin a = NULL;
    int m;
    if (N > 0) {
        m = rand() % N;
        a = newABin(v[m], RandArvFromArray(v, m),
                    RandArvFromArray(v + m + 1, N - m - 1));
    }
    return a;
}

// Questão 1
ABin removeMenor(ABin *a) {
    if ((*a) == NULL)
        return NULL;

    ABin ant = NULL, cursor = *a;

    while (cursor->esq != NULL) {
        ant = cursor;
        cursor = cursor->esq;
    }

    if (ant == NULL) {
        (*a) = cursor->dir;
    } else {
        ant->esq = cursor->dir;
    }

    cursor->dir = NULL;
    cursor->esq = NULL;

    return cursor;
}

void removeRaiz(ABin *a) {
    if ((*a) == NULL)
        return;
    if ((*a)->esq == NULL) {
        ABin tmp = (*a);
        (*a) = (*a)->dir;
        free(tmp);
        return;
    }
    ABin ant = NULL, cursor = (*a)->esq;

    while (cursor->dir != NULL) {
        ant = cursor;
        cursor = cursor->dir;
    }

    if (ant == NULL) {
        ABin temp = (*a);
        (*a) = cursor;
        cursor->dir = temp->dir;
        free(temp);
    } else {
        ant->dir = cursor->esq;
        cursor->esq = (*a)->esq;
        cursor->dir = (*a)->dir;
        ABin temp = (*a);
        (*a) = cursor;
        free(temp);
    }
}

int removeElem(ABin *a, int x) { return (-1); }

// Questão 2
void rodaEsquerda(ABin *a) {
    ABin b = (*a)->dir;
    (*a)->dir = b->esq;
    b->esq = (*a);
    *a = b;
}
void rodaDireita(ABin *a) {
    ABin b = (*a)->esq;
    (*a)->esq = b->dir;
    b->dir = *a;
    *a = b;
}
void promoveMenor(ABin *a) {}
void promoveMaior(ABin *a) {}
ABin removeMenorAlt(ABin *a) { return NULL; }

// Questão 3
int constroiEspinhaAux(ABin *a, ABin *ult) { return (-1); }
int constroiEspinha(ABin *a) {
    ABin ult;
    return (constroiEspinhaAux(a, &ult));
}

ABin equilibraEspinha(ABin *a, int n) { return NULL; }

void equilibra(ABin *a) {}

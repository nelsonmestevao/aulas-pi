#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "abin.h"

void imprimeArvore(ABin a) {
    if (a == NULL) {
        return;
    }

    imprimeArvore(a->esq);
    printf("%d -> ", a->valor);
    imprimeArvore(a->dir);
};

int main() {
    printf("\n\n___________     Testes     ___________\n\n");

    //       (23)
    //          \
    //           \
    //            \
    //             (73)
    ABin c_73 = newABin(73, NULL, NULL);
    ABin c_23 = newABin(23, NULL, c_73);

    // ABin menor_c = removeMenor(&c_23);

    // assert(menor_c->valor == 23);

    removeRaiz(&c_23);

    imprimeArvore(c_23);

    printf("\n--------\n");

    //       (27)
    //      /   \
    //     /     \
    //    /       \
    //  (17)       (73)
    //    \
    //     \
    //     (19)

    ABin a_19 = newABin(19, NULL, NULL);
    ABin a_17 = newABin(17, NULL, a_19);

    ABin a_73 = newABin(73, NULL, NULL);

    ABin a_27 = newABin(27, a_17, a_73);

    // ABin menor_a = removeMenor(&a_27);

    // assert(menor_a->valor == 17);

    removeRaiz(&a_27);

    imprimeArvore(a_27);

    printf("\n--------\n");

    //            (12)
    //           /   \
    //          /     \
    //         /       \
    //       (6)       (21)
    //       / \        /
    //      /   \      /
    //    (3)   (7)  (16)
    //    /
    //   /
    //  (2)
    ABin b_2 = newABin(2, NULL, NULL);
    ABin b_3 = newABin(3, b_2, NULL);
    ABin b_7 = newABin(7, NULL, NULL);
    ABin b_6 = newABin(6, b_3, b_7);

    ABin b_16 = newABin(16, NULL, NULL);
    ABin b_21 = newABin(21, b_16, NULL);

    ABin b_12 = newABin(12, b_6, b_21);

    // ABin menor_b = removeMenor(&b_12);

    // assert(menor_b->valor == 2);

    removeRaiz(&b_12);
    imprimeArvore(b_12);

    printf("\n\n___________ Fim dos testes ___________\n\n");
    return 0;
}
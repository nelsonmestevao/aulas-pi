#include <assert.h>
#include <stdio.h>

float multInt1(int n, float m) {
    float r = 0;

    int i;
    for (i = 0; i < n; i++) {
        r += m; // r = r + m;
    }

    return r;
}

float multInt2(int n, float m) {
    float r = 0;
    int c = 0;

    while (n > 0) {
        c++;
        if (n % 2 != 0) {
            r += m;
            c++;
        }

        n /= 2;
        m *= 2;
        c += 2;
    }

    printf("Existiram %i operações\n", c);

    return r;
}

int mdc1(int a, int b) {
    int r;
    int menor = (a > b) ? b : a;

    int i;
    for (i = 1; i <= menor; i++) {
        if (a % i == 0 && b % i == 0) {
            r = i;
        }
    }

    return r;
}

int mdc1_b(int a, int b) {
    int r;
    int menor = (a > b) ? b : a;

    int i;
    for (i = menor; i >= 1; i--) {
        if (a % i == 0 && b % i == 0) {
            return i;
        }
    }

    return r;
}

int mdc2(int a, int b) {
    while (a != 0 && b != 0) {
        if (a > b) {
            a -= b; // a = a - b;
        } else if (a < b) {
            b -= a; // b = b - a;
        } else {
            return a;
        }
    }

    if (a == 0)
        return b;

    return a;
}

int mdc3(int a, int b) {
    while (a != 0 && b != 0) {
        if (a > b) {
            a %= b;
        } else if (a < b) {
            b %= a;
        } else {
            return a;
        }
    }

    if (a == 0)
        return b;

    return a;
}

int fib1(int n) {
    if (n < 2) {
        return 1;
    }

    return fib1(n - 1) + fib1(n - 2);
}

int fib2(int n) {
    int acc1, acc2;
    acc1 = acc2 = 1;

    int i;
    for (i = 2; i <= n; i++) {
        // tmp = acc2;
        // acc2 = acc2 + acc1;
        // acc1 = tmp;

        acc2 += acc1;
        acc1 = acc2 - acc1;
    }

    return acc2;
}

int main() {
    float e1 = multInt1(81, 423);
    assert(e1 == 34263);

    float e2 = multInt2(81, 423);
    assert(e2 == 34263);

    int e3 = mdc1(126, 45);
    assert(e3 == 9);

    int e3_b = mdc1_b(126, 45);
    assert(e3_b == 9);

    int e4 = mdc2(126, 45);
    assert(e4 == 9);

    int e5 = mdc3(126, 45);
    assert(e5 == 9);

    assert(1 == fib1(0));
    assert(1 == fib1(1));
    assert(2 == fib1(2));
    assert(3 == fib1(3));
    assert(5 == fib1(4));
    assert(8 == fib1(5));
    assert(13 == fib1(6));

    assert(1 == fib2(0));
    assert(1 == fib2(1));
    assert(2 == fib2(2));
    assert(3 == fib2(3));
    assert(5 == fib2(4));
    assert(8 == fib2(5));
    assert(13 == fib2(6));

    return 0;
}
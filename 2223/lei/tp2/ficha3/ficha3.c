/* https://estev.ao/pi-f3 */
#include <assert.h>
#include <stdio.h>

/* https://estev.ao/pi2223 */

void swapM(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

void swap(int v[], int i, int j) {
    int temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

int soma(int v[], int N) {
    int total = 0;
    int i;
    for (i = 0; i < N; i++) {
        /* total = total+v[i]; */
        total += v[i];
    }
    return total;
}

int maximum(int v[], int N, int *m) {
    if (N < 1) {
        return -1;
    }

    *m = v[0];

    int i;
    for (i = 1; i < N; i++) {
        if (v[i] > *m) {
            *m = v[i];
        }
    }
    return 0;
}

int quadrados(int q[], int N) {
    q[0] = 0;
    int i;
    for (i = 1; i < N; i++) {
        q[i] = q[i - 1] + 2 * (i - 1) + 1;
        // q[i] = i * i;
    }
}

int main() {
    // int c = 10;
    // int d = 51;

    // swapM(&c, &d);

    // assert(c == 51);
    // assert(d == 10);

    // int x[10] = {1,2,3,4,5,6,7,8,9,10};

    // int r = soma(x, 10);

    // assert(r == 55);

    // int ns[5] = {103, 20, 89, 12, 109};

    // int *m = malloc(sizeof(int));
    // int m;
    // int r = maximum(ns, 5, &m);
    // if (r == 0) {
    //     printf("O máximo é %d\n", m);
    // } else {
    //     printf("O array não tem elementos!\n");
    // }

    int qds[10];

    quadrados(qds, 10);

    int j;
    for (j = 0; j < 10; j++) {
        printf("%d\t", qds[j]);
    }
    printf("\n");

    return 0;
}
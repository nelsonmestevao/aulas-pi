// https://estev.ao/pi-f4
// https://estev.ao/pi2223
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

int eVogal(char c) {
    c = tolower(c);

    if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y') {
        return 1;
    }

    // switch(c) {
    //     case 'a':
    //     case 'e':
    //     case 'i':
    //     case 'o':
    //     case 'u':
    //     case 'y':
    //         return 1;
    //     default:
    //         return 0;
    // }

    return 0;
}

int contaVogais(char *s) {
    int pos = 0;
    int r = 0;

    while (s[pos] != '\0') {
        if (eVogal(s[pos])) {
            r++;
        }

        pos++;
    }

    // while(*s != '\0') {
    //     if (eVogal(*s)) {
    //         r++;
    //     }

    //     s++;
    // }

    return r;
}

int retiraVogaisRep(char *s) {
    int repetidasContador = 0;
    int tamanho = strlen(s);
    char aux[tamanho];
    int pos = 1;
    int posAux = 1;

    aux[0] = s[0];

    // copiar sem repetidos
    while (s[pos] != '\0') {
        if (eVogal(s[pos]) && s[pos] == aux[posAux - 1]) {
            repetidasContador++;
        } else {
            aux[posAux] = s[pos];
            posAux++;
        }

        pos++;
    }
    aux[posAux] = '\0';

    posAux = 0;
    // voltar a copiar para a string original
    while (aux[posAux] != '\0') {
        s[posAux] = aux[posAux];
        posAux++;
    }

    s[posAux] = '\0';

    return repetidasContador;
}

int retiraVogaisRep2(char *s) {
    int i, j, c = 0;
    for (i = 0; i < strlen(s); i++) {
        if (eVogal(s[i])) {
            if (s[i] == s[i + 1]) {
                c++;
                for (j = i; j < strlen(s); j++) {
                    s[j] = s[j + 1];
                }
                i--;
            }
        }
    }
    return c;
}

int retiraVogaisRep3(char *s) {
    char *n = s;
    char *p = n + 1;

    while (*p != '\0') {
        if (!(eVogal(*p) && *n == *p)) {
            n++;
            *n = *p;
        }

        p++;
    }

    *(n + 1) = '\0';

    return p - n - 1;
}

int main() {
    assert(3 == contaVogais("A cobra"));
    assert(10 == contaVogais("Programacao Imperativa"));
    assert(0 == contaVogais("mskr"));

    char s[7] = "coobra";
    int r = retiraVogaisRep3(s);
    assert(r == 1);
    printf("%s\n", s);

    return 0;
}
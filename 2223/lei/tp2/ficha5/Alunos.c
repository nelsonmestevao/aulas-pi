#include "Alunos.h"
#include <stdio.h>

// https://estev.ao/pi-f5
// https://estev.ao/pi2223

void dumpV(int v[], int N) {
    int i;
    for (i = 0; i < N; i++)
        printf("%d ", v[i]);
}
void imprimeAluno(Aluno *a) {
    int i;
    printf("%-5d %s (%d", a->numero, a->nome, a->miniT[0]);
    for (i = 1; i < 6; i++)
        printf(", %d", a->miniT[i]);
    printf(") %5.2f %d\n", a->teste, nota(*a));
}

int nota(Aluno a) {
    // 20% mini-testes
    // 80% teste
    // mini testes tem nota minima de 25% (3 pontos em 12 pontos)

    int i;
    int totalMiniTestes = 0;
    for (i = 0; i < 6; i++) {
        totalMiniTestes += a.miniT[i];
    }

    if (totalMiniTestes < 3) {
        return 0;
    }

    float media = ((totalMiniTestes * 10) / 6) * 0.2 + a.teste * 0.8;

    if (media < 9.5) {
        return 0;
    }

    return (int)(media + 0.5); // ou utilizar a função round do math.h
}

int procuraNum(int num, Aluno t[], int N) {
    int i;
    for (i = 0; i < N; i++) {
        if (t[i].numero > num) {
            return -1;
        }
        if (t[i].numero == num) {
            return i;
        }
    }
    return -1;
}

void swap_aluno(Aluno v[], int i, int j) {
    Aluno temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

void ordenaPorNum(Aluno t[], int N) {
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N - i - 1; j++) {
            if (t[j].numero > t[j + 1].numero) {
                swap_aluno(t, j, j + 1);
            }
        }
    }
}
int procuraNumInd(int num, int ind[], Aluno t[], int N) { return -1; }

void criaIndPorNum(Aluno t[], int N, int ind[]) {}
void criaIndPorNome(Aluno t[], int N, int ind[]) {}

// void imprimeTurmaInd (int ind[], Aluno t[], int N){
//     imprimeTurmaInd_sol(ind,t,N);
// }

#include "Deque.h"
#include <stdio.h>
#include <stdlib.h>

void initDeque(Deque *q) {
    q->front = NULL;
    q->back = NULL;
}

int DisEmpty(Deque q) { return q.front == NULL || q.back == NULL; }

int pushBack(Deque *q, int x) {
    DList new = newDList(x, NULL);
    if (new == NULL) {
        return 1;
    }

    if (DisEmpty(*q)) {
        q->front = new;
        q->back = new;
    } else {
        new->ant = q->back;
        q->back = new;
    }

    return 0;
}

int pushFront(Deque *q, int x) {
    DList new = newDList(x, NULL);
    if (new == NULL) {
        return 1;
    }

    if (DisEmpty(*q)) {
        q->front = new;
        q->back = new;
    } else {
        new->prox = q->front;
        q->front = new;
    }

    return 0;
}

int popBack(Deque *q, int *x) {
    if (DisEmpty(*q)) {
        return 1;
    }

    *x = q->back->valor;
    DList tmp = q->back;
    q->back = q->back->ant;
    free(tmp);
    if (q->back == NULL) {
        q->front = NULL;
    } else {
        q->back->prox = NULL;
    }

    return 0;
}

int popFront(Deque *q, int *x) {
    if (DisEmpty(*q)) {
        return 1;
    }

    *x = q->front->valor;
    DList tmp = q->front;
    q->front = q->front->prox;
    free(tmp);
    if (q->front == NULL) {
        q->back = NULL;
    } else {
        q->front->ant = NULL;
    }

    return 0;
}

int popMax(Deque *q, int *x) { return -1; }
int back(Deque q, int *x) {
    if (DisEmpty(q)) {
        return 1;
    }
    *x = q.back->valor;
    return 0;
}
int front(Deque q, int *x) {
    if (DisEmpty(q)) {
        return 1;
    }
    *x = q.front->valor;
    return 0;
}

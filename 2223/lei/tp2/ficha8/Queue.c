#include "Queue.h"
#include <stdio.h>
#include <stdlib.h>

void initQueue(Queue *q) {
    q->inicio = NULL;
    q->fim = NULL;
}

int QisEmpty(Queue q) { return q.inicio == NULL; }

int enqueue(Queue *q, int x) {
    LInt new = newLInt(x, NULL);

    if (new == NULL) {
        return 1;
    }

    if (q->fim == NULL) {
        q->fim = new;
    } else {
        q->fim->prox = new;
        q->fim = new;
    }

    if (q->inicio == NULL) {
        q->inicio = new;
    }

    return 0;
}

int dequeue(Queue *q, int *x) {
    if (QisEmpty(*q)) {
        return 1;
    }

    *x = q->inicio;
    LInt temp = q->inicio;
    q->inicio = q->inicio->prox;
    free(temp);

    return 0;
}

int frontQ(Queue q, int *x) {
    if (QisEmpty(q)) {
        return 1;
    }

    *x = q.inicio->valor;

    return 0;
}

typedef LInt QueueC;

void initQueueC(QueueC *q);
int QisEmptyC(QueueC q) { return -1; }
int enqueueC(QueueC *q, int x) { return -1; }
int dequeueC(QueueC *q, int *x) { return -1; }
int frontC(QueueC q, int *x) { return -1; }

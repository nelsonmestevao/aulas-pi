#include "Stack.h"
#include <stdio.h>
#include <stdlib.h>

void initStack(Stack *s) { *s = NULL; }

int SisEmpty(Stack s) { return s == NULL; }

int push(Stack *s, int x) {
    LInt new = newLInt(x, *s);
    if (new == NULL) {
        return 1;
    }
    *s = new;
    return 0;
}

int pop(Stack *s, int *x) {
    if (*s == NULL)
        return 1;
    LInt temp = *s;
    *x = temp->valor;
    *s = temp->prox;
    free(temp);

    return 0;
}

int top(Stack s, int *x) {
    if (s == NULL)
        return 1;
    *x = s->valor;
    return 0;
}

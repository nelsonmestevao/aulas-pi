#include <stdio.h>
#include <stdlib.h>

#include "Listas.h"

void fun(LInt *q) {
    (*q) = newLInt(27, NULL);
    /* q->valor = 89; */
}

void foo(int *x) { (*x) = 10; }

int main() {
    LInt q = newLInt(5, NULL);

    printf("Antes: %d\n", q->valor);
    fun(&q);
    printf("Depois: %d\n", q->valor);

    int x = 25;

    printf("Antes: %d\n", x);
    foo(&x);
    printf("Depois: %d\n", x);

    return 0;
}

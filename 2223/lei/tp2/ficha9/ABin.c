#include "ABin.h"

ABin newABin(ABin *a, int r, ABin e, ABin d) {
    (*a) = malloc(sizeof(struct nodo));
    if (a != NULL) {
        a->valor = r;
        a->esq = e;
        a->dir = d;
    }
    // return a;
}

ABin RandArvFromArray(int v[], int N) {
    ABin a = NULL;
    int m;
    if (N > 0) {
        m = rand() % N;
        a = newABin(v[m], RandArvFromArray(v, m),
                    RandArvFromArray(v + m + 1, N - m - 1));
    }
    return a;
}

// https://manual.cs50.io/
int max(int a, int b) { return a >= b ? a : b; }

int altura(ABin a) {
    int ae, ad;
    if (a == NULL)
        return 0;
    ae = altura(a->esq);
    ad = altura(a->dir);
    return 1 + max(ae, ad);
}

int altura(ABin a) {
    if (a == NULL)
        return 0;

    return 1 + max(altura(a->esq), altura(a->dir));
}

int isLeaf(ABin a) { return a->esq == NULL && a->dir == NULL; }

int nFolhas(ABin a) {
    if (a == NULL)
        return 0;
    if (isLeaf(a)) {
        return 1;
    }
    return nFolhas(a->esq) + nFolhas(a->dir);
}

// ABin maisEsquerda(ABin a) {
//     if (a == NULL || a->esq == NULL) {
//         return a;
//     }

//     return maisEsquerda(a->esq);
// }

ABin maisEsquerda(ABin a) {
    if (a == NULL)
        return NULL;

    while (a->esq != NULL)
        a = a->esq;

    return a;
}

void imprimeNivel(ABin a, int l) {
    if (a != NULL) {
        if (l == 0) {
            printf("> %d\n", a->valor);
        } else {
            imprimeNivel(a->esq, l - 1);
            imprimeNivel(a->dir, l - 1);
        }
    }
}

int procuraE(ABin a, int x) {
    if (a == NULL)
        return 0;

    if (a->valor == x) {
        return 1;
    }

    return procuraE(a->esq, x) || procuraE(a->dir, x);
}

// struct nodo *procura(ABin a, int x) {
//     if (a == NULL) return NULL;

//     if (a->valor == x) return a;

//     if (x < a->valor) {
//         return procura(a->esq, x);
//     } else {
//         return procura(a->dir, x);
//     }
// }

struct nodo *procura(ABin a, int x) {
    if (a == NULL)
        return NULL;

    while (a != NULL && a->valor != x) {
        if (a->valor < x)
            a = a->esq;
        else
            a = a->dir;
    }

    return a;
}

int nivel(ABin a, int x) {
    int c = 0;

    while (a != NULL) {
        if (a->valor == x)
            return c;
        else if (a->valor < x) {
            a = a->dir;
        } else {
            a = a->esq;
        }

        c++;
    }

    return -1;
}

int nivel_aux(ABin a, int x, int c) {
    if (a == NULL)
        return -1;

    if (a->valor == x)
        return c;

    if (a->valor < x) {
        return nivel_aux(a->dir, x, c + 1);
    } else {
        return nivel_aux(a->esq, x, c + 1);
    }
}

int nivel2(ABin a, int x) { return nivel_aux(a, x, 0); }

void imprimeAte(ABin a, int x) {
    if (a == NULL) {
        return;
    }

    imprimeAte(a->esq, x);

    if (a->valor < x) {
        printf("%d -> ", a->valor);
        imprimeAte(a->dir, x);
    }
};
#include "ABin.h"
#include <assert.h>
#include <time.h>

// https://estev.ao/pi-f9

int main() {
    //       (6)
    //      /   \
    //     /     \
    //    /       \
    //  (12)     (21)
    //           /
    //          /
    //         (4)
    ABin a4 = newABin(4, NULL, NULL);
    ABin a21 = newABin(21, a4, NULL);
    ABin a12 = newABin(12, NULL, NULL);
    ABin a6 = newABin(6, a12, a21);
    assert(altura(a6) == 3);

    //            (12)
    //           /   \
//          /     \
//         /       \
//       (6)       (21)
    //       / \        /
    //      /   \      /
    //    (3)   (7)  (16)
    //    /
    //   /
    //  (2)
    ABin b_2 = newABin(2, NULL, NULL);
    ABin b_3 = newABin(3, b_2, NULL);
    ABin b_7 = newABin(7, NULL, NULL);
    ABin b_6 = newABin(6, b_3, b_7);

    ABin b_16 = newABin(16, NULL, NULL);
    ABin b_21 = newABin(21, b_16, NULL);

    ABin b_12 = newABin(12, b_6, b_21);

    assert(nivel(b_12, 6) == 1);
    assert(nivel(b_12, 16) == 2);
    assert(nivel2(b_12, 6) == 1);
    assert(nivel2(b_12, 16) == 2);

    imprimeAte(b_12, 22);
}

// int main() {
//     int v1[15] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29},
//         v2[15] = {21, 3, 15, 27, 9, 11, 23, 5, 17, 29, 1, 13, 25, 7, 19},
//         N = 15;
//     ABin a1, a2, r;

//     srand(time(NULL));

//     printf("_______________ Testes _______________\n\n");
//     // N = rand() % 16;
//     a1 = RandArvFromArray(v2, N);
//     printf("Primeira árvore de teste (%d elementos)\n", N);
//     dumpABin(a1, N);

//     printf("altura = %d\n", altura(a1));
//     printf("numero de folhas: %d\n", nFolhas(a1));
//     printf("Nodo mais à esquerda: ");
//     r = maisEsquerda(a1);
//     if (r == NULL)
//         printf("(NULL)\n");
//     else
//         printf("%d\n", r->valor);
//     printf("Elementos no nivel 3_______\n");
//     imprimeNivel(a1, 3);
//     printf("\n___________________________\n");

//     printf("procura de 2: %d\n", procuraE(a1, 2));
//     printf("procura de 9: %d\n", procuraE(a1, 9));

//     freeABin(a1);

//     // N = rand() % 16;
//     a2 = RandArvFromArray(v1, N);
//     printf("\nSegunda árvore de teste (%d elementos)\n", N);
//     dumpABin(a2, N);

//     printf("procura de 9: ");
//     r = procura(a2, 9);
//     if (r == NULL)
//         printf("(NULL)\n");
//     else
//         printf("%d\n", r->valor);
//     printf("procura de 2: ");
//     r = procura(a2, 2);
//     if (r == NULL)
//         printf("(NULL)\n");
//     else
//         printf("%d\n", r->valor);
//     printf("nível do elemento 2: %d\n", nivel(a2, 2));
//     printf("nível do elemento 9: %d\n", nivel(a2, 9));
//     imprimeAte(a2, 20);

//     freeABin(a1);

//     printf("\n\n___________ Fim dos testes ___________\n\n");
//     return 0;
// }
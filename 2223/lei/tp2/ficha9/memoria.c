#include <stdio.h>
#include <stdlib.h>

typedef struct aluno {
    int numero;
    double nota;
} *Aluno;

void exemplo(Aluno *b) {
    Aluno a = malloc(sizeof(struct aluno));
    a->numero = 76434;
    a->nota = 18.0;
    (*b) = a;
    // b->numero = a->numero;
    // b->nota = a->nota;
}

int main() {
    Aluno x;

    exemplo(&x);

    printf("Número: %i\n", x->numero);
    printf("Nota: %f\n", x->nota);

    return 0;
}
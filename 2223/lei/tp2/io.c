#include <stdio.h>

// void duplica(int *x) {
//     (*x) = (*x) * 2;
//     printf("%i\n", *x);
// }

int main() {
    // int x = 5;

    // duplica(&x);

    // printf("%i\n", x);

    int i;

    printf("Dá-me um número: ");
    scanf("%i", &i);

    printf("Eu também gosto muito do número %i!\n", i);

    return 0;
}
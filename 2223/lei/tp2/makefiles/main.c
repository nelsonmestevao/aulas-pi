#include <stdio.h>

#include "mathematics.h"

int main() {
    int n;

    printf("Dá-me um número: ");
    scanf("%i", &n);

    int r = fib(n);

    printf("\nfib(%i) = %i\n", n, r);

    return 0;
}
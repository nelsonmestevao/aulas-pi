int fib(int n) {
    int acc1 = 1, acc2 = 1;

    int i;
    for (i = 2; i <= n; i++) {
        acc2 += acc1;
        acc1 = acc2 - acc1;
    }

    return acc2;
}
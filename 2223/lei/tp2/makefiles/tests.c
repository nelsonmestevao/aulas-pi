#include <assert.h>

#include "mathematics.h"

void test_fib() {
    assert(1 == fib(0));
    assert(1 == fib(1));
    assert(2 == fib(2));
    assert(3 == fib(3));
    assert(5 == fib(4));
    assert(8 == fib(5));
    assert(13 == fib(6));
}

int main() {
    test_fib();

    return 0;
}
#include <stdio.h>
#include <stdlib.h>

// splint -retvalint -hints -noret main.c
//
// gdb ./a.out --tui
// run
// start; s
//
// valgrind ./a.out
//
// memusage --data="mem.dat" --png="mem.png" ./a.out
//
// valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all
// --track-origins=yes --log-file="mem.log" ./a.out
//
// gprof a.out gmon.out > analysis.txt

static int bar() {
    int *p = NULL;
    p = malloc(sizeof(int));
    if (p != NULL)
        *p = 42;
    free(p);
}

static void foo() { bar(); }

int main() {
    foo();
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int x() { return 2; }

int main() {
    char *ptr = malloc(sizeof(char) * 6);
    x();
    if (ptr != NULL) {
        strcpy(ptr, "nelson");
        printf("Hello, %s\n", ptr);
        free(ptr);
    }
    /* free(ptr); */
    return 0;
}

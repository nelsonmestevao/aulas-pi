#include <math.h>
#include <stdio.h>

int distancia(int x, int y, int z, int w) {
    // ponto1: (x, y)
    // ponto2: (z, w)
    return (int)sqrt(pow(x - z, 2) + pow(y - w, 2));
}

int circulo(int r) {
    int diametro = 2 * r;
    int x, y;
    int c = 0; // contador de '#'

    for (y = 0; y <= diametro; y++) {
        for (x = 0; x <= diametro; x++) {
            if (distancia(x, y, r, r) <= r) {
                putchar('#');
                c++;
            } else {
                putchar(' ');
            }
        }

        putchar('\n');
    }

    return c;
}

int main() {
    int r;

    printf("Dá-me o raio: ");
    scanf("%d", &r);

    int c = circulo(r);

    printf("Para desenhar este circulo foram precisos %d '#'\n", c);

    return 0;
}
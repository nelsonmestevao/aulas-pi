#include <stdio.h>

// formatado com o clang-format
// $ clang-format -i ficha1.c

void quadrado(int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            putchar('#');
        }

        putchar('\n');
    }
}

void xadrez(int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if ((j + i) % 2 == 0) {
                putchar('_');
            } else {
                putchar('#');
            }
        }

        putchar('\n');
    }
}

void triangulo(int n) {
    int i, j;

    for (i = 1; i <= n; i++) {
        for (j = 1; j <= i; j++) {
            putchar('#');
        }
        putchar('\n');
    }

    for (i = n - 1; i > 0; i--) {
        for (j = 1; j <= i; j++) {
            putchar('#');
        }
        putchar('\n');
    }
}

void triangulo2(int n) {
    int i, j, k;

    for (i = 1; i <= n; i++) {
        for (j = i; j < n; j++) {
            putchar(' ');
        }
        for (k = 1; k <= i * 2 - 1; k++) {
            putchar('#');
        }
        putchar('\n');
    }
}

// Compilar:
// $ gcc ficha1.c -o programa
int main() {
    int n;

    printf("Altura Triangulo: ");
    scanf("%d", &n);

    triangulo2(n);

    return 0;
}
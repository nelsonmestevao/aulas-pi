#include <stdio.h>

void triangulo(int n) {
    int i, j, k;

    for (i = 1; i <= n; i++) {
        for (j = i; j < n; j++) {
            putchar(' ');
        }
        for (k = 1; k <= i * 2 - 1; k++) {
            putchar('#');
        }
        putchar('\n');
    }
}
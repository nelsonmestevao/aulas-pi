#include <assert.h>
#include <stdio.h>

float multInt1(int n, float m) {
    float r = 0;

    int i;

    for (i = 0; i < n; i++) {
        r = r + m; // r += m;
    }

    return r;
}

float multInt2(int n, float m) {
    float soma = 0.0;
    int c = 0;

    while (n > 0) {
        c++;
        if (n % 2 == 1) {
            soma += m; // soma = soma + m;
            c++;
        }

        n >>= 1;
        m *= 2;
        c += 2;
    }

    printf("Operações: %d\n", c);

    return soma;
}

int mdc1(int a, int b) {
    int menor;

    if (a < b) {
        menor = a;
    } else {
        menor = b;
    }

    int r = 1;
    int i;
    for (i = 1; i <= menor; i++) {
        if (a % i == 0 && b % i == 0) {
            r = i;
        }
    }

    return r;
}

int mdc1_b(int a, int b) {
    int menor = (a < b) ? a : b;

    int i;
    for (i = menor; i > 1; i--) {
        if (a % i == 0 && b % i == 0) {
            return i;
        }
    }

    return 1;
}

int mdc2_rec(int a, int b) {
    if (a == b) {
        return a;
    }

    if (a > b) {
        return mdc2_rec(a - b, b);
    }

    return mdc2_rec(a, b - a);
}

int mdc2(int a, int b) {
    while (a != 0 && b != 0) {
        if (a > b) {
            a %= b;
        } else if (b > a) {
            b %= a;
        } else {
            return a;
        }
    }

    if (a == 0) {
        return b;
    }

    return a;
}

int fib_rec(int n) {
    if (n <= 2)
        return 1;

    return fib_rec(n - 1) + fib_rec(n - 2);
}

int fib(int n) {
    int acc1, acc2;
    acc1 = acc2 = 1;

    int i;
    for (i = 3; i <= n; i++) {
        // int r = acc2;
        // acc2 = acc2 + acc1;
        // acc1 = r;

        acc2 = acc2 + acc1;
        acc1 = acc2 - acc1;
    }

    return acc2;
}

int main() {
    // assert(multInt1(2, 3.0) == 6.0);
    // assert(multInt1(81, 423.0) == 34263.0);
    // assert(multInt1(25, 25) == 625);
    // assert(multInt1(35, 35) == 1225);

    // assert(multInt2(2, 3.0) == 6.0);
    // assert(multInt2(81, 423.0) == 34263.0);
    // assert(multInt2(25, 25) == 625);
    // assert(multInt2(35, 35) == 1225);

    // assert(mdc1(4, 6) == 2);
    // assert(mdc1(24, 16) == 8);
    // assert(mdc1(126, 45) == 9);

    // assert(mdc1_b(4, 6) == 2);
    // assert(mdc1_b(24, 16) == 8);
    // assert(mdc1_b(126, 45) == 9);

    // assert(mdc2_rec(4, 6) == 2);
    // assert(mdc2_rec(24, 16) == 8);
    // assert(mdc2_rec(126, 45) == 9);

    // assert(mdc2(4, 6) == 2);
    // assert(mdc2(24, 16) == 8);
    // assert(mdc2(126, 45) == 9);

    // assert(fib_rec(1) == 1);
    // assert(fib_rec(2) == 1);
    // assert(fib_rec(3) == 2);
    // assert(fib_rec(4) == 3);
    // assert(fib_rec(5) == 5);
    // assert(fib_rec(6) == 8);
    // assert(fib_rec(7) == 13);

    // assert(fib(1) == 1);
    // assert(fib(2) == 1);
    // assert(fib(3) == 2);
    // assert(fib(4) == 3);
    // assert(fib(5) == 5);
    // assert(fib(6) == 8);
    // assert(fib(7) == 13);

    int n = 70;
    printf("fib(%d) = %d\n", n, fib(n));

    return 0;
}
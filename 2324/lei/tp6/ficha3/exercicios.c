#include <stdio.h>

void swapM(int *a, int *b) {
    int tmp = (*a);
    (*a) = (*b);
    (*b) = tmp;
}

void swap(int v[], int i, int j) {
    int tmp = v[i];
    v[i] = v[j];
    v[j] = tmp;
}

int maximum(int v[], int N, int *m) {
    if (N == 0) {
        return -1;
    }

    (*m) = v[0];

    int i;
    for (i = 1; i < N; i++) {
        if (v[i] > (*m)) {
            (*m) = v[i];
        }
    }

    return 0;
}

void pascal_rec(int v[], int N) {
    if (N == 1) {
        v[0] = 1;
    } else {
        int ant[N - 1];
        pascal_rec(ant, N - 1);

        v[0] = 1;
        v[N - 1] = 1;

        int i;
        for (i = 1; i < N - 1; i++) {
            v[i] = ant[i - 1] + ant[i];
        }
    }
}

void pascal(int v[], int N) {
    pascal_rec(v, N);

    int i;
    for (i = 0; i < N; i++) {
        printf("%d ", v[i]);
    }
    printf("\n");
}

int main() {
    int x = 3, y = 5;

    swapM(&x, &y);

    printf("x = %d; y = %d\n", x, y);

    int v[4] = {6, 12, 3, 9};
    int m;

    if (maximum(v, 4, &m) == 0) {
        printf("%d\n", m);
    } else {
        printf("Não foi encontrado máximo!\n");
    }

    int p[5];

    pascal(p, 5);

    return 0;
}
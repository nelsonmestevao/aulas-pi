#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

int eVogal(char l) {
    l = tolower(l);

    return (l == 'a' || l == 'e' || l == 'i' || l == 'o' || l == 'u' ||
            l == 'y');
}

int contaVogais(char *s) {
    int c = 0;
    int i;

    for (i = 0; s[i] != '\0'; i++) {
        if (eVogal(s[i]))
            c++;
    }

    return c;
}

int retiraVogaisRep1(char *s) {
    int tamanho = strlen(s);
    char aux[tamanho + 1];

    aux[0] = s[0];

    int c = 0;
    int i;
    int j = 1;
    for (i = 1; i < tamanho; i++) {
        if (eVogal(s[i]) && s[i] == s[i - 1]) {
            c++;
        } else {
            aux[j++] = s[i];
            // equivalente a:
            // aux[j] = s[i];
            // j++;
        }
    }

    for (i = 0; i < j; i++) {
        s[i] = aux[i];
    }

    s[j] = '\0';

    return c;
}

int retiraVogaisRep2(char *s) {
    char *n = s;
    char *p = n + 1;

    while (*p != '\0') {
        if (!(eVogal(*p) && *p == *n)) {
            n++;
            (*n) = (*p);
        }

        p++;
    }

    *(n + 1) = '\0';

    return p - n - 1;
}

int duplicaVogais(char *s) { return 0; }

int main() {
    assert(contaVogais("Ola") == 2);
    assert(10 == contaVogais("Programacao Imperativa"));

    char s1[100] = "Estaa e umaa string coom duuuplicadoos";
    char s2[100] = "Estaa e umaa string coom duuuplicadoos";
    int x;

    printf("Testes\n");
    // printf("A string \"%s\" tem %d vogais\n", s1, contaVogais(s1));

    x = retiraVogaisRep1(s1);
    printf("Foram retiradas %d vogais, resultando em \"%s\"\n", x, s1);

    x = retiraVogaisRep2(s2);
    printf("Foram retiradas %d vogais, resultando em \"%s\"\n", x, s2);

    // x = duplicaVogais(s1);
    // printf("Foram acrescentadas %d vogais, resultando em \"%s\"\n", x, s1);

    printf("\nFim dos testes\n");

    return 0;
}
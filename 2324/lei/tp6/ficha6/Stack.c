#include "Stack.h"
#include <stdio.h>
#include <stdlib.h>

// Static stacks

void SinitStack(SStack s) { s->sp = 0; }

int SisEmpty(SStack s) { return s->sp == 0; }

int Spush(SStack s, int x) {
    if (s->sp == Max) {
        return 1;
    }

    s->values[s->sp++] = x;

    return 0;
}

int Spop(SStack s, int *x) {
    if (SisEmpty(s)) {
        return 1;
    }

    *x = s->values[--(s->sp)];

    return 0;
}

int Stop(SStack s, int *x) {
    if (SisEmpty(s)) {
        return 1;
    }

    *x = s->values[s->sp - 1];

    return 0;
}

void ShowSStack(SStack s) {
    int i;
    printf("%d Items: ", s->sp);
    for (i = s->sp - 1; i >= 0; i--)
        printf("%d ", s->values[i]);
    putchar('\n');
}

// Stacks with dynamic arrays

int dupStack(DStack s) {
    // int r = 0, i;
    // int *t = malloc(2 * s->size * sizeof(int));

    // if (t == NULL)
    //     r = 1;
    // else {
    //     for (i = 0; i < s->size; i++)
    //         t[i] = s->values[i];
    //     free(s->values);
    //     s->values = t;
    //     s->size *= 2;
    // }
    // return r;

    int *t = realloc(s->values, 2 * s->size * sizeof(int));

    if (t == NULL)
        return 1;

    s->values = t;
    s->size *= 2;

    return 0;
}

void DinitStack(DStack s) {
    s->sp = 0;
    s->size = Max;
    s->values = malloc(s->size * sizeof(int));
}

int DisEmpty(DStack s) { return s->sp == 0; }

int Dpush(DStack s, int x) {
    if (s->sp == s->size) {
        if (dupStack(s) != 0) {
            return 1;
        }
    }

    s->values[s->sp++] = x;

    return 0;
}

int Dpop(DStack s, int *x) {
    if (DisEmpty(s)) {
        return 1;
    }

    *x = s->values[--(s->sp)];

    return 0;
}

int Dtop(DStack s, int *x) {
    if (DisEmpty(s)) {
        return 1;
    }

    *x = s->values[s->sp - 1];

    return 0;
}

void ShowDStack(DStack s) {
    int i;
    printf("%d Items: ", s->sp);
    for (i = s->sp - 1; i >= 0; i--)
        printf("%d ", s->values[i]);
    putchar('\n');
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct celula {
    char *palavra;
    int ocorr;
    struct celula *prox;
} *Palavras;

void libertaLista(Palavras);
int quantasP(Palavras);
void listaPal(Palavras);
char *ultima(Palavras);
Palavras acrescentaInicio(Palavras, char *);
Palavras acrescentaFim(Palavras, char *);
Palavras acrescenta(Palavras, char *);
struct celula *maisFreq(Palavras);

void libertaLista(Palavras l) {
    while (l != NULL) {
        Palavras aux = l;
        l = l->prox;
        free(aux->palavra);
        free(aux);
    }
}

int quantasP(Palavras l) {
    int c = 0;
    while (l != NULL) {
        l = l->prox;
        c++;
    }
    return c;
}

void listaPal(Palavras l) {
    while (l != NULL) {
        printf("+ %s (%d)\n", l->palavra, l->ocorr);
        l = l->prox;
    }
}
char *ultima(Palavras l) {
    /* if (l == NULL) return NULL;

    if (l->prox == NULL) {
    return l->palavra;
    }

    return ultima(l->prox); */
    if (l == NULL)
        return NULL;

    while (l->prox != NULL) {
        l = l->prox;
    }

    return l->palavra;
}
Palavras acrescentaInicio(Palavras l, char *p) {
    Palavras novo = malloc(sizeof(struct celula));

    if (novo == NULL)
        return l;

    novo->palavra = strdup(p);
    novo->ocorr = 1;
    novo->prox = l;

    return novo;
}
Palavras acrescentaFim(Palavras l, char *p) {
    if (l == NULL)
        return acrescentaInicio(l, p);

    Palavras cabeca = l;

    while (l->prox != NULL) {
        l = l->prox;
    }

    Palavras novo = malloc(sizeof(struct celula));
    if (novo != NULL) {
        novo->palavra = strdup(p);
        novo->ocorr = 1;
        novo->prox = NULL;
    }

    l->prox = novo;

    return cabeca;
}

Palavras acrescenta(Palavras l, char *p) {
    if (l == NULL || strcmp(l->palavra, p) > 0) {
        // tenho de acrescentar no inicio da lista
        return acrescentaInicio(l, p);
    }

    Palavras cabeca = l;
    Palavras ant = l;

    while (l != NULL) {
        // encontrei celula com palavra igual
        if (strcmp(l->palavra, p) == 0) {
            l->ocorr++;
            return cabeca;
        }

        // encontrei primeira celula que a palavra é alfabeticamente superior
        if (strcmp(l->palavra, p) > 0) {
            Palavras novo = acrescentaInicio(l, p);
            ant->prox = novo;
            return cabeca;
        }

        ant = l;
        l = l->prox;
    }

    // nao encontrei nenhuma palavra maior do que p
    Palavras novo = acrescentaInicio(NULL, p);
    ant->prox = novo;

    return cabeca;
}

// Palavras acrescenta(Palavras l, char *p) {
//     Palavras cabeca = l;
//     Palavras ant = NULL;

//     while (l != NULL) {
//         // encontrei celula com palavra igual
//         if (strcmp(l->palavra, p) == 0) {
//             l->ocorr++;
//             return cabeca;
//         }

//         // encontrei primeira celula que a palavra é alfabeticamente superior
//         if (strcmp(l->palavra, p) > 0) {
//             Palavras novo = acrescentaInicio(l, p);
//             if (ant != NULL) {
//                 ant->prox = novo;
//             }
//             return cabeca;
//         }

//         ant = l;
//         l = l->prox;
//     }

//     if (l == NULL || ant == NULL)
//         return acrescentaInicio(NULL, p);

//     ant->prox = acrescentaInicio(l, p);

//     return cabeca;
// }

struct celula *maisFreq(Palavras l) {
    struct celula *maior = NULL;
    if (l != NULL)
        maior = l;

    while (l != NULL) {
        if (l->ocorr > maior->ocorr) {
            maior = l;
        }

        l = l->prox;
    }

    return maior;
}

Palavras inicializa(Palavras dic) {
    char *canto1[44] = {
        "as",          "armas",      "e",        "os",         "baroes",
        "assinalados", "que",        "da",       "ocidental",  "praia",
        "lusitana",    "por",        "mares",    "nunca",      "de",
        "antes",       "navegados",  "passaram", "ainda",      "alem",
        "da",          "taprobana",  "em",       "perigos",    "e",
        "guerras",     "esforcados", "mais",     "do",         "que",
        "prometia",    "a",          "forca",    "humana",     "e",
        "entre",       "gente",      "remota",   "edificaram", "novo",
        "reino",       "que",        "tanto",    "sublimaram"};

    int i;
    struct celula *p;
    for (i = 0; i < 44; i++)
        dic = acrescentaInicio(dic, canto1[i]);

    return dic;
}

int main() {
    Palavras dic = NULL;

    char *canto1[44] = {
        "as",          "armas",      "e",        "os",         "baroes",
        "assinalados", "que",        "da",       "ocidental",  "praia",
        "lusitana",    "por",        "mares",    "nunca",      "de",
        "antes",       "navegados",  "passaram", "ainda",      "alem",
        "da",          "taprobana",  "em",       "perigos",    "e",
        "guerras",     "esforcados", "mais",     "do",         "que",
        "prometia",    "a",          "forca",    "humana",     "e",
        "entre",       "gente",      "remota",   "edificaram", "novo",
        "reino",       "que",        "tanto",    "sublimaram"};

    printf("\n_____________ Testes _____________\n\n");

    int i;
    struct celula *p;
    // for (i = 0; i < 44; i++)
    //     dic = acrescentaInicio(dic, canto1[i]);

    // printf("Foram inseridas %d palavras\n", quantasP(dic));
    // printf("palavras existentes:\n");
    // listaPal(dic);
    // printf("última palavra inserida: %s\n", ultima(dic));

    // libertaLista(dic);

    dic = NULL;

    srand(42);

    for (i = 0; i < 1000; i++) {
        char *palavra = canto1[rand() % 44];
        printf("a acrescentar %s...\n", palavra);
        dic = acrescenta(dic, palavra);
    }

    printf("Foram inseridas %d palavras\n", quantasP(dic));
    printf("palavras existentes:\n");
    listaPal(dic);
    printf("última palavra inserida: %s\n", ultima(dic));

    p = maisFreq(dic);
    printf("Palavra mais frequente: %s (%d)\n", p->palavra, p->ocorr);

    printf("\n_________ Fim dos testes _________\n\n");

    return 0;
}

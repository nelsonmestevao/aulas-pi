#include "Deque.h"
#include <stdio.h>
#include <stdlib.h>

void initDeque(Deque *q) {}

int DisEmpty(Deque q) {
    return q.front == NULL; // q.back == NULL;
}

int pushBack(Deque *q, int x) { return -1; }
int pushFront(Deque *q, int x) { return -1; }
int popBack(Deque *q, int *x) { return -1; }
int popFront(Deque *q, int *x) { return -1; }

int popMax(Deque *q, int *x) {
    if (DisEmpty(*q)) {
        return 1;
    }

    DList maximo = q->front;
    DList t = q->front;
    while (t->prox != NULL) {
        if (maximo->valor < t->valor) {
            maximo = t;
        }
        t = t->prox;
    }

    if (maximo->ant != NULL) {
        maximo->ant->prox = maximo->prox;
    } else {
        return popFront(q, x);
    }

    if (maximo->prox != NULL) {
        maximo->prox->ant = maximo->ant;
    } else {
        return popBack(q, x);
    }

    (*x) = maximo->valor;

    free(maximo);

    return 0;
}

int back(Deque q, int *x) { return -1; }
int front(Deque q, int *x) { return -1; }

// Teste 22/23

/////// Questão 3
/*
Apresente uma definição da função int depth(ABin a, int x)
que devolve o menor nível a que um elemento x se encontra
na árvore (ou -1 se x não se encontra na árvore).
Considere a definição usual do tipo ABin.
Considere ainda que a raiz se encontra no nível 0.
*/

ABin fromArray(int v[], int N) {
    ABin r = NULL;
    int m = N / 2;
    if (N > 0) {
        r = malloc(sizeof(struct abin_nodo));
        r->valor = v[m];
        r->esq = fromArray(v, m);
        r->dir = fromArray(v + m + 1, N - m - 1);
    }
    return r;
}

void freeABin(ABin a) {
    if (a) {
        freeABin(a->esq);
        freeABin(a->dir);
        free(a);
    }
}

int depth_rec(ABin a, int x, int nivel) {
    if (a == NULL)
        return (-1);

    if (a->valor == x)
        return nivel;

    int nivel_esq = depth_rec(a->esq, x, nivel + 1);
    int nivel_dir = depth_rec(a->dir, x, nivel + 1);

    if (nivel_esq == -1)
        return nivel_dir;
    if (nivel_dir == -1)
        return nivel_esq;

    return nivel_esq < nivel_dir ? nivel_esq : nivel_dir;
}

int depth(ABin a, int x) { return depth_rec(a, x, 0); }

/////// Questão 4
/*
Implemente a função int wordle(char secreta[], char tentativa[])
que dada uma palavra secreta que se pretende descobrir e uma
tentativa com o mesmo tamanho devolve o número de caracteres
na palavra tentativa em que o utilizador já acertou.
Ambas as palavras só contêm letras minúsculas.
A função deve também modificar a tentativa substituindo todas
as letras que não tem correspondente na palavra secreta
por um '*' e convertendo para maiúscula as letras que estão
na posição certa.
Por exemplo,
se a palavra secreta for "laranja" e a tentativa for "cerejas"
a função deve devolver 1 e alterar a tentativa para "**R*ja*"
(apenas o 'r' está na posição certa e os caracteres 'j' e 'a'
aparecem no segredo noutras posições).
Se a tentativa for "bananas" a função deve devolver 3 e alterar
a tentativa para "*A*ANa*".

*/

int conta_char(char secreta[], char tentativa[], char x) {
    int i = 0;
    int c = 0;
    while (secreta[i] != '\0') {
        if (secreta[i] == x && tentativa[i] != toupper(secreta[i]))
            c++;
    }
    return c;
}

int elem_repetidos(char x, char tentativa[]) {
    int c = 0;

    while (palavra[i] != "\0") {
        if (palavra[i] == x)
            c++;
        i++;
    }

    return c;
}

int elem(char x, char palavra[]) {
    int i = 0;
    while (palavra[i] != "\0") {
        if (palavra[i] == x)
            return 1;
        i++;
    }

    return 0;
}

int wordle(char secreta[], char tentativa[]) {
    int i;
    for (i = 0; i < strlen(tentativa); i++) {
        if (!elem(tentativa[i], secreta)) {
            tentativa[i] = '*';
        }
    }
    for (i = 0; i < strlen(tentativa) && i < strlen(secreta); i++) {
        if (tentativa[i] == secreta[i]) {
            tentativa[i] = toupper(tentativa[i]);
        }
    }

    for (i = 0; i < strlen(tentativa) && i < strlen(secreta); i++) {
        int pos_erradas = conta_char(secreta, palavra, palavra[i]);
        int ten_erradas = elem_repetidos(palavra[i], tentativa);

        if (ten_erradas > pos_erradas) {
            palavra[i] = '*';
        }
    }

    return -1;
}

/////// Questão 5
/*
Implemente a função LInt periodica(char s[]) que dada uma string
com uma sequência infinita periódica de dígitos constrói uma
lista (circular) com esses dígitos.
Assuma que a parte da sequência que se repete indefinidamente
está representada entre parêntesis e aparece sempre no final
da string.
Assuma também a definição usual do tipo LInt.
Por exemplo, se a string for "34(56)" deverá ser construída
a seguinte lista:
--> 3 --> 4 --> 5 --> 6 -\
                ^        |
                |        |
                \________/

*/
LInt newLInt(int x, LInt xs) {
    LInt r = malloc(sizeof(struct lint_nodo));
    if (r) {
        r->valor = x;
        r->prox = xs;
    }
    return r;
}

void freeLInt(LInt l) {
    LInt t;
    while (l) {
        t = l;
        l = l->prox;
        free(t);
    }
}

LInt circular(char s[]) {
    LInt r; // a cabeça
    int i = 1;
    r = newLInt(s[0] - '0', NULL);
    LInt *ap = &(r->prox);
    while (s[i] != ')') {
        *ap = newLInt(s[i] - '0', NULL);
            ant = &((*ap)->prox));
            i++;
    }
    *ap = r;
    return r;
}

LInt periodica(char s[]) {
    LInt r = newLInt(s[0] - '0', NULL);
    int i = 1;
    LInt *ap = &(r->prox);
    while (s[i] != '(') {
        *ap = newLInt(s[i] - '0', NULL);
        *ap = &((*ap)->prox);
        i++;
    }
    (*ap) = circular(s + i);
    return r;
}

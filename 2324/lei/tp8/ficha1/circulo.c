#include <math.h>
#include <stdio.h>

int distancia(int x, int y, int z, int w) {
    // ponto 1: (x, y)
    // ponto 2: (z, w)
    return (int)sqrt(pow(x - z, 2) + pow(y - w, 2));
}

int circulo(int raio) {
    int diametro = 2 * raio;

    int c = 0; // contador
    int x, y;

    for (y = 0; y <= diametro; y++) {
        for (x = 0; x <= diametro; x++) {
            if (distancia(x, y, raio, raio) <= raio) {
                putchar('#');
                c++;
            } else {
                putchar(' ');
            }
        }
        putchar('\n');
    }

    return c;
}

void circunferencia(int raio) {
    int diametro = 2 * raio;

    int x, y;

    for (y = 0; y <= diametro; y++) {
        for (x = 0; x <= diametro; x++) {
            if (distancia(x, y, raio, raio) == raio) {
                putchar('#');
            } else {
                putchar(' ');
            }
        }
        putchar('\n');
    }
}

int main() {
    int r;

    printf("Dá-me o raio: ");
    scanf("%d", &r);

    int c = circulo(r);

    printf("Este círculo usou %d cardinais!\n", c);

    return 0;
}
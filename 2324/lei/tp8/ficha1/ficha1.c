#include <stdio.h>

void quadrado(int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            putchar('#');
        }

        putchar('\n');
    }
}

void xadrez(int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if ((j + i) % 2 == 0) {
                putchar('#');
            } else {
                putchar('_');
            }
        }

        putchar('\n');
    }
}

void triangulo(int n) {
    int i, j;

    for (j = 1; j <= n; j++) {
        for (i = 1; i <= j; i++) {
            putchar('#');
        }
        putchar('\n');
    }

    for (j = n - 1; j > 0; j--) {
        for (i = j; i > 0; i--) {
            putchar('#');
        }
        putchar('\n');
    }
}

void triangulo2(int n) {
    int i, j, k;

    for (i = 1; i <= n; i++) {
        for (j = n - i; j > 0; j--) {
            putchar(' ');
        }
        for (k = 1; k <= (i * 2 - 1); k++) {
            if (i > 2) {
                if (k % 2 == 0) {
                    putchar('O');
                } else {
                    putchar('*');
                }
            } else {
                putchar('*');
            }
        }
        putchar('\n');
    }
}

int main() {
    int n;

    printf("Altura Triangulo: ");
    scanf("%d", &n);

    triangulo2(n);

    return 0;
}
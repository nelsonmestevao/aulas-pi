#include <stdio.h>

void triangulo(int);

int main() {
    int n;

    printf("Altura Triangulo: ");
    scanf("%d", &n);

    triangulo(n);
    return 0;
}
#include <assert.h>
#include <stdio.h>

float multInt1(int n, float m) {
    int i;
    float soma = 0;

    for (i = 1; i <= n; i++) {
        soma += m;
    }

    return soma;
}

float multInt2(int n, float m) {
    float soma = 0;
    int c = 0;

    while (n > 0) {
        c++;
        if (n % 2 == 1) {
            soma += m; // soma = soma + m;
            c++;
        }

        n >>= 1; // n = n / 2;
        m *= 2;  // m = m * 2;
        c += 2;
    }

    printf("N.º Operações: %d\n", c);

    return soma;
}

int mdc1(int a, int b) {
    int r;
    int n;
    int menor = (a < b) ? a : b;

    for (n = menor; n > 1; n--) {
        if (a % n == 0 && b % n == 0)
            return n;
    }

    return 1;
}

int mdc2_rec(int a, int b) {
    if (a == 0) {
        return b;
    }

    if (b == 0) {
        return a;
    }

    if (a > b) {
        return mdc2_rec(a - b, b);
    } else if (a < b) {
        return mdc2_rec(a, b - a);
    } else {
        return a;
    }
}

int mdc2(int a, int b) {
    while (a != 0 && b != 0) {
        if (a > b) {
            a %= b;
        } else if (a < b) {
            b %= a;
        } else {
            return a;
        }
    }

    if (a == 0) {
        return b;
    }

    return a;
}

int fib1(int n) {
    if (n < 2) {
        return 1;
    }

    return fib1(n - 1) + fib1(n - 2);
}

int fib2(int n) {
    int acc1, acc2;
    acc1 = acc2 = 1;

    int i;
    for (i = 2; i <= n; i++) {
        acc2 = acc2 + acc1;
        acc1 = acc2 - acc1;
    }

    return acc2;
}

int main() {
    int n = 2;
    float m = 3.0;

    float resultado = multInt1(n, m);

    assert(resultado == 6);

    n = 81;
    m = 423;

    float resultado2 = multInt2(n, m);

    assert(resultado2 == 34263);

    int resultado3 = mdc1(4, 6);

    assert(resultado3 == 2);

    assert(mdc1(40, 45) == 5);

    assert(mdc2_rec(4, 6) == 2);
    assert(mdc2_rec(40, 45) == 5);

    assert(mdc2(4, 8) == 4);
    assert(mdc2(4, 6) == 2);
    assert(mdc2(40, 45) == 5);
    assert(mdc2(19, 7) == 1);

    assert(fib1(0) == 1);
    assert(fib1(1) == 1);
    assert(fib1(2) == 2);
    assert(fib1(3) == 3);
    assert(fib1(4) == 5);
    assert(fib1(5) == 8);

    assert(fib2(0) == 1);
    assert(fib2(1) == 1);
    assert(fib2(2) == 2);
    assert(fib2(3) == 3);
    assert(fib2(4) == 5);
    assert(fib2(5) == 8);

    // Testar tempo de execução com `$ time ./a.out`
    // int n = 40;
    // printf("fib1(%d) = %d\n", n, fib1(n));

    // int n = 40;
    // printf("fib2(%d) = %d\n", n, fib2(n));

    return 0;
}
#include <stdio.h>

void swapM(int *a, int *b) {
    int tmp = (*a);
    (*a) = (*b);
    (*b) = tmp;
}

void swap(int v[], int i, int j) {
    int tmp = v[i];
    v[i] = v[j]; // *(v + sizeof(int) * i) = *(v + sizeof(int) * j)
    v[j] = tmp;
}

int maximum(int v[], int N, int *m) {
    if (N == 0) {
        return -1;
    }

    (*m) = v[0];

    int i;

    for (i = 1; i < N; i++) {
        if (v[i] > (*m)) {
            (*m) = v[i];
        }
    }

    return 0;
}

void quadrados(int q[], int N) {
    q[0] = 0;
    int i;
    for (i = 1; i < N; i++)
        q[i] = q[i - 1] + 2 * (i - 1) + 1;
}

void pascal(int v[], int T) {
    if (T == 1) {
        v[0] = 1;
        printf("1\n");
    } else {
        int ant[T - 1];
        pascal(ant, T - 1);

        v[0] = 1;
        v[T - 1] = 1;

        int i;
        for (i = 1; i < T - 1; i++) {
            v[i] = ant[i] + ant[i - 1];
        }

        int j;
        for (j = 0; j < T; j++) {
            printf("%d ", v[j]);
        }
        printf("\n");
    }
}

#define N 5

int main() {
    int x = 3, y = 5;

    swapM(&x, &y);

    printf("x = %d; y = %d\n", x, y);

    int v[N] = {42, 73, 12, 1, 101};
    int m;

    if (maximum(v, N, &m) == 0) {
        printf("O maior da lista é %d\n", m);
    } else {
        printf("A função maximum falhou!\n");
    }

    int q[N];
    quadrados(q, N);

    int i;

    for (i = 0; i < N; i++) {
        printf("%d ", q[i]);
    }
    printf("\n");

    int bn[N];

    pascal(bn, N);

    return 0;
}

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

int eVogal(char l) {
    l = tolower(l);
    return (l == 'a' || l == 'e' || l == 'i' || l == 'o' || l == 'u' ||
            l == 'y');
}

int contaVogais(char *s) {
    int i;
    int c = 0;

    for (i = 0; s[i] != '\0'; i++) {
        if (eVogal(s[i])) {
            c++;
        }
    }

    // while (*s != '\0') {
    //     if (eVogal(*s)) {
    //         c++;
    //     }
    //     s++;
    // }

    return c;
}

int retiraVogaisRep1(char *s) {
    int tamanho = strlen(s);
    char aux[tamanho + 1];
    int c = 0;

    int i;
    int j = 1;

    aux[0] = s[0];

    for (i = 1; i < tamanho; i++) {
        if (!(eVogal(s[i]) && s[i] == s[i - 1])) {
            aux[j++] = s[i];
        } else {
            c++;
        }
    }

    for (i = 1; i < j; i++) {
        s[i] = aux[i];
    }

    s[i] = '\0';

    return c;
}

int retiraVogaisRep2(char *s) {
    char *n = s;
    char *p = n + 1;

    while (*p != '\0') {
        if (!(eVogal(*p) && *p == *n)) {
            n++;
            *n = *p;
        }

        p++;
    }

    *(n + 1) = '\0';

    return p - n - 1;
}

int duplicaVogais(char *s) { return 0; }

int main() {
    char *s = "Olaa";

    assert(3 == contaVogais(s));
    assert(1 == contaVogais("PI"));

    // char s1[100] = "Estaa e umaa string coom duuuplicadoos";
    char s2[100] = "Estaa e umaa string coom duuuplicadoos";
    int x;

    printf("Testes\n");
    // printf("A string \"%s\" tem %d vogais\n", s1, contaVogais(s1));

    // x = retiraVogaisRep1(s1);
    // printf("Foram retiradas %d vogais, resultando em \"%s\"\n", x, s1);

    x = retiraVogaisRep2(s2);
    printf("Foram retiradas %d vogais, resultando em \"%s\"\n", x, s2);

    // x = duplicaVogais(s1);
    // printf("Foram acrescentadas %d vogais, resultando em \"%s\"\n", x, s1);

    printf("\nFim dos testes\n");

    return 0;
}
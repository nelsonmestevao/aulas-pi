#include <math.h>
#include <stdio.h>
#include <string.h>

#include "Alunos.h"

void dumpV(int v[], int N) {
    int i;
    for (i = 0; i < N; i++)
        printf("%d ", v[i]);
}
void imprimeAluno(Aluno *a) {
    int i;
    printf("%-5d %s (%d", a->numero, a->nome, a->miniT[0]);
    for (i = 1; i < 6; i++)
        printf(", %d", a->miniT[i]);
    printf(") %5.2f %d\n", a->teste, nota(*a));
}

int nota(Aluno a) {
    float mediaMiniTestes;
    int somaMiniTestes = 0;
    int i;

    for (i = 0; i < NUM_MINI_TESTES; i++) {
        somaMiniTestes += a.miniT[i];
    }

    mediaMiniTestes = somaMiniTestes / NUM_MINI_TESTES;

    if (mediaMiniTestes < 5) {
        return 0;
    }

    float mediaPonderada = 0.8 * a.teste + 0.2 * mediaMiniTestes;

    int notaFinal = (int)mediaPonderada + 0.5;

    // if (notaFinal < 10) {
    //     return 0;
    // }

    // return notaFinal;

    return notaFinal < 10 ? 0 : notaFinal;
}

int procuraNum(int num, Aluno t[], int N) {
    int i;

    // for (i = 0; i < N; i++) {
    //     if (t[i].numero == num) {
    //         return i;
    //     } else if (t[i].numero > num) {
    //         return -1;
    //     }
    // }

    for (i = 0; i < N && t[i].numero <= num; i++) {
        if (t[i].numero == num) {
            return i;
        }
    }

    return -1;
}

void swap_aluno(Aluno t[], int i, int j) {
    Aluno tmp = t[i];
    t[i] = t[j];
    t[j] = tmp;
}

void ordenaPorNum(Aluno t[], int N) {
    int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N - i - 1; j++) {
            if (t[j].numero > t[j + 1].numero) {
                swap_aluno(t, j, j + 1);
            }
        }
    }
}

// int swap(int *i, int *j) {
//     int * tmp;
//     *tmp = *i;
//     *i = *j;
//     *j = *tmp;
// }

void swap(int v[], int i, int j) {
    int tmp = v[i];
    v[i] = v[j];
    v[j] = tmp;
}

int procuraNumInd(int num, int ind[], Aluno t[], int N) {}

void criaIndPorNum(Aluno t[], int N, int ind[]) {
    int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N - i - 1; j++) {
            if (t[ind[j]].numero > t[ind[j + 1]].numero) {
                swap(ind, j, j + 1);
            }
        }
    }
}

void criaIndPorNome(Aluno t[], int N, int ind[]) {
    int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N - i - 1; j++) {
            if (strcmp(t[ind[j]].nome, t[ind[j + 1]].nome) > 0) {
                swap(ind, j, j + 1);
            }
        }
    }
}

void imprimeTurmaInd(int ind[], Aluno t[], int N) {
    /* imprimeTurmaInd_sol(ind, t, N); */
}

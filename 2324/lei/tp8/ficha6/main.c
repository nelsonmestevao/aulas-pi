#include <stdio.h>

#include "Queue.h"
#include "Stack.h"

int main() {
    int i;
    // struct staticStack s1;
    // SStack S1 = &s1;
    // struct dinStack d1;
    // DStack D1 = &d1;

    struct staticQueue q1;
    SQueue Q1 = &q1;
    struct dinQueue r1;
    DQueue R1 = &r1;

    // printf("Testing Stacks .... \n");
    // SinitStack(S1);
    // DinitStack(D1);
    // for (i = 0; i < 15; i++) {
    //     if (Spush(S1, i) != 0)
    //         printf("ERROR pushing %d\n", i);
    //     if (Dpush(D1, i) != 0)
    //         printf("ERROR pushing %d\n", i);
    // }
    // ShowSStack(S1);
    // ShowDStack(D1);

    printf("Testing Queues .... \n");
    // SinitQueue(Q1);
    DinitQueue(R1);
    for (i = 0; i < 15; i++) {
        // if (Senqueue(Q1, i) != 0)
        //     printf("ERROR enqueueing %d\n", i);
        if (i == 0) {
            printf("I = 0; front = %d; size = % d; length = %d\n", R1->front,
                   R1->size, R1->length);
        }
        if (Denqueue(R1, i) != 0)
            printf("ERROR enqueueing %d\n", i);
    }
    // ShowSQueue(Q1);
    ShowDQueue(R1);

    int x;
    Ddequeue(R1, &x);
    ShowDQueue(R1);
    Denqueue(R1, 73);
    ShowDQueue(R1);

    return 0;
}

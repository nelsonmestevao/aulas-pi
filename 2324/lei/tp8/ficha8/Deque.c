#include "Deque.h"
#include <stdio.h>
#include <stdlib.h>

void initDeque(Deque *q) {
    q->front = NULL;
    q->back = NULL;
}

int DisEmpty(Deque q) {
    return q.front == NULL; // q.back == NULL;
}

int pushBack(Deque *q, int x) {
    LInt novo = newDList(x, NULL);
    if (novo == NULL) {
        return 1;
    }
    if (DisEmpty(*q)) {
        q->front = novo;
    }

    novo->ant = q->back;
    q->back = novo;

    return 0;
}

int pushFront(Deque *q, int x) {
    LInt novo = newDList(x, q->front);

    if (novo == NULL)
        return 1;

    if (DisEmpty(*q)) {
        q->back = novo;
    }

    q->front = novo;

    return 0;
}

int popBack(Deque *q, int *x) {
    if (DisEmpty(*q)) {
        return 1;
    }

    LInt ultimo = q->back;

    (*x) = ultimo->valor;

    q->back = ultimo->ant;

    if (q->front == ultimo) {
        q->front = NULL;
    }

    free(ultimo);

    return 0;
}

int popFront(Deque *q, int *x) {
    if (DisEmpty(*q)) {
        return 1;
    }

    LInt cabeca = q->front;

    (*x) = cabeca->valor;

    q->front = cabeca->prox;

    if (q->back == cabeca) {
        q->back = NULL;
    }

    free(cabeca);

    return 0;
}

int popMax(Deque *q, int *x) {
    if (DisEmpty(*q))
        return 1;

    int max = q->front->valor;
    LInt maximo = q->front;

    LInt tmp = q->front->prox;

    while (tmp != NULL) {
        if (tmp->valor > max) {
            max = tmp->valor;
            maximo = tmp;
        }
        tmp = tmp->prox;
    }

    if (maximo->ant != NULL) {
        maximo->ant->prox = maximo->prox;
    } else {
        q->front = maximo->prox;
    }

    if (maximo->prox != NULL) {
        maximo->prox->ant = maximo->ant;
    } else {
        q->back = maximo->ant;
    }

    free(maximo);

    return 0;
}

int back(Deque q, int *x) {
    if (DisEmpty(q)) {
        return 1;
    }

    (*x) = q.back->valor;

    return 0;
}

int front(Deque q, int *x) {
    if (DisEmpty(q)) {
        return 1;
    }

    (*x) = q.front->valor;

    return 0;
}

// Teste 21/22

/*
Questão 4:
Implemente a função void inc(char s[]) que, dada uma
string s com um número em decimal, incrementa esse
número numa unidade.
Assuma que a string tem espaço suficiente para
armazenar o número resultante.
Por exemplo, se a string for "123" deverá ser
modificada para "124".
Se for "199" deverá ser modificada para "200".
*/
void inc(char s[]) {
    int len = strlen(s);
    int i;
    int r = 0;
    for (i = len - 1; i >= 0 && !r; i--) {
        if (s[i] == '9') {
            s[i] = '0';
        } else {
            s[i] = s[i] + 1;
            r = 1;
        }
    }
}

// Teste 22/23

/////// Questão 3
/*
Apresente uma definição da função int depth(ABin a, int x)
que devolve o menor nível a que um elemento x se encontra
na árvore (ou -1 se x não se encontra na árvore).
Considere a definição usual do tipo ABin.
Considere ainda que a raiz se encontra no nível 0.
*/

ABin fromArray(int v[], int N) {
    ABin r = NULL;
    int m = N / 2;
    if (N > 0) {
        r = malloc(sizeof(struct abin_nodo));
        r->valor = v[m];
        r->esq = fromArray(v, m);
        r->dir = fromArray(v + m + 1, N - m - 1);
    }
    return r;
}

void freeABin(ABin a) {
    if (a) {
        freeABin(a->esq);
        freeABin(a->dir);
        free(a);
    }
}
int depth_rec(ABin a, int x, int nivel) {
    if (a == NULL)
        return (-1);

    int nivel_esq = depth_rec(a->esq, x, nivel + 1);
    int nivel_dir = depth_rec(a->dir, x, nivel + 1);

    if (nivel_esq == -1)
        return nivel_dir;
    if (nivel_dir == -1)
        return nivel_esq;

    return nivel_esq < nivel_dir ? nivel_esq : nivel_dir;
}

int depth(ABin a, int x) {
    if (a == NULL)
        return (-1);

    if (a->valor == x)
        return 0;

    return depth_rec(a, x, 1);
}
/////// Questão 4
/*
Implemente a função int wordle(char secreta[], char tentativa[])
que dada uma palavra secreta que se pretende descobrir e uma
tentativa com o mesmo tamanho devolve o número de caracteres
na palavra tentativa em que o utilizador já acertou.
Ambas as palavras só contêm letras minúsculas.
A função deve também modificar a tentativa substituindo todas
as letras que não tem correspondente na palavra secreta
por um '*' e convertendo para maiúscula as letras que estão
na posição certa.
Por exemplo,
se a palavra secreta for "laranja" e a tentativa for "cerejas"
a função deve devolver 1 e alterar a tentativa para "**R*ja*"
(apenas o 'r' está na posição certa e os caracteres 'j' e 'a'
aparecem no segredo noutras posições).
Se a tentativa for "bananas" a função deve devolver 3 e alterar
a tentativa para "*A*ANa*".

*/

int esta_presente(char x, char palavra[]) {
    int i;
    int c = 0;
    for (i = 0; palavra[i] != '\0'; i++) {
        if (palavra[i] == x)
            c++;
    }
    return c;
}

int conta_char_sitio_errado(char secreta[], char tentativa[], char x) {
    int i;
    int c = 0;
    for (i = 0; secreta[i] != '\0'; i++) {
        if (secreta[i] == x && tentativa[i] != toupper(secreta[i]))
            c++;
    }
    return c;
}

void marca_asterisco(char tentativa[], char x) {
    int i;
    int marcou = 0;
    for (i = 0; tentativa[i] != '\0' && !marcou; i++) {
        if (tentativa[i] == x) {
            tentativa[i] = '*';
            marcou = 1; // break;
        }
    }
}

int wordle(char secreta[], char tentativa[]) {
    int certas = 0;
    int i;
    // mascarar letras não presentes na palavra secreta
    for (i = 0; i < strlen(tentativa); i++) {
        if (!esta_presente(tentativa[i], secreta)) {
            tentativa[i] = '*';
        }
    }

    // passar a maiuscula os que são iguais e estão no sitio certo
    for (i = 0; secreta[i] != '\0' && tentativa[i] != '\0'; i++) {
        if (tentativa[i] == secreta[i]) {
            tentativa[i] = toupper(tentativa[i]);
            certas++;
        }
    }

    for (i = 0; secreta[i] != '\0' && tentativa[i] != '\0'; i++) {
        int pos_erradas =
            conta_char_sitio_errado(secreta, tentativa, secreta[i]);
        int ten_erradas = esta_presente(secreta[i], tentativa);

        while (ten_erradas > pos_erradas) {
            marca_asterisco(tentativa, secreta[i]);
            ten_erradas = esta_presente(secreta[i], tentativa);
        }
    }

    return certas;
}

/////// Questão 5
/*
Implemente a função LInt periodica(char s[]) que dada uma string
com uma sequência infinita periódica de dígitos constrói uma
lista (circular) com esses dígitos.
Assuma que a parte da sequência que se repete indefinidamente
está representada entre parêntesis e aparece sempre no final
da string.
Assuma também a definição usual do tipo LInt.
Por exemplo, se a string for "34(56)" deverá ser construída
a seguinte lista:
--> 3 --> 4 --> 5 --> 6 -\
                ^        |
                |        |
                \________/

*/
LInt newLInt(int x, LInt xs) {
    LInt r = malloc(sizeof(struct lint_nodo));
    if (r) {
        r->valor = x;
        r->prox = xs;
    }
    return r;
}

void freeLInt(LInt l) {
    LInt t;
    while (l) {
        t = l;
        l = l->prox;
        free(t);
    }
}

LInt criarLista(char s[]) {
    LInt cabeca = NULL;
    cabeca = newLInt(s[0] - '0', NULL);
    LInt *er = &(cabeca->prox);
    int i = 1;
    LInt novo;
    LInt abrir_parenteses;
    while (s[i] != '\0') {
        if (s[i] == '(') {
            i++;
            novo = newLint(s[i] - '0', NULL);
            abrir_parenteses = novo;
        } else if (s[i] == ')') {
            *er = abrir_parenteses;
        } else {
            novo = newLint(s[i] - '0', NULL);
        }
        *er = &(novo->prox);
        i++;
    }
    return cabeca;
}

LInt circular(char s[]) {
    LInt r, *er;
    int i = 1;
    r = newLInt(s[0] - '0', NULL);
    er = &(r->prox);
    while (s[i] != ')') {
        *er = newLInt(s[i] - '0', NULL);
        er = &((*er)->prox);
        i++;
    }
    *er = r;

    return r;
}

LInt periodica(char s[]) {
    //...
    return NULL;
}

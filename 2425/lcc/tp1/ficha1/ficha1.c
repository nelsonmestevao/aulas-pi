#include <stdio.h>

void quadrado(int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            putchar('#');
        }
        putchar('\n');
    }
}

void linha_underscore(int n) {
    int i;
    for (i = 0; i < n; i++) {
        if (i % 2 == 0)
            putchar('_');
        else
            putchar('#');
    }
}

void linha_cardinal(int n) {
    int i;
    for (i = 0; i < n; i++) {
        if (i % 2 == 0)
            putchar('#');
        else
            putchar('_');
    }
}

void xadrez1(int n) {
    int i;
    for (i = 0; i < n; i++) {
        if (i % 2 == 0) {
            linha_underscore(n);
        } else {
            linha_cardinal(n);
        }
    }
    putchar('\n');
}

void linha_chars(int n, char a, char b) {
    int i;
    for (i = 0; i < n; i++) {
        if (i % 2 == 0)
            putchar(a);
        else
            putchar(b);
    }
    putchar('\n');
}

void xadrez2(int n) {
    int i;
    for (i = 0; i < n; i++) {
        if (i % 2 == 0) {
            linha_chars(n, '_', '#');
        } else {
            linha_chars(n, '#', '_');
        }
    }
}

void xadrez(int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if ((i + j) % 2 == 0) {
                putchar('#');
            } else {
                putchar('_');
            }
        }
        putchar('\n');
    }
}

void trianguloH(int n) {
    int i, j;
    for (i = 1; i <= n; i++) {
        for (j = 0; j < i; j++) {
            putchar('#');
        }
        putchar('\n');
    }
    for (i = n - 1; i > 0; i--) {
        for (j = 0; j < i; j++) {
            putchar('#');
        }
        putchar('\n');
    }
}

void linha(int x, char c) {
    int i;
    for (i = 0; i < x; i++)
        putchar(c);
}

void trianguloV(int n) {
    int i, j;
    for (i = 1; i <= n; i++) {
        linha(n - i, ' ');
        linha(2 * i - 1, '#');
        putchar('\n');
    }
}

void trianguloV2(int n) {
    int i, j;
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= 2 * n - 1; j++) {
            if ((j <= n - i) || ((n + i) <= j)) {
                putchar(' ');
            } else {
                putchar('#');
            }
        }
        putchar('\n');
    }
}

int circulo(int raio) {
    printf("Circulo %d ainda não está feito...\n", raio);
    return 0;
}

int main() {
    // quadrado(5);
    // printf("Xadrez de Tamanho 5:\n");
    // xadrez(5);
    // printf("Triangulo Horizontal de tamanho 5:\n");
    // trianguloH(5);
    printf("Triangulo Vertical de tamanho 5:\n");
    trianguloV2(5);
    // trianguloV(5);
    printf("\nForam usados %d caracteres para fazer o circulo\n", circulo(5));
    return 0;
}

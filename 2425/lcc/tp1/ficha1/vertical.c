#include <stdio.h>

void linha(int x, char c) {
    int i;
    for (i = 0; i < x; i++)
        putchar(c);
}

void triangulo(int n) {
    int i, j;
    for (i = 1; i <= n; i++) {
        linha(n - i, ' ');
        linha(2 * i - 1, '#');
        putchar('\n');
    }
}

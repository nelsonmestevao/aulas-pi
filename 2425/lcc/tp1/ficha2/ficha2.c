#include <assert.h>
#include <stdio.h>

int ePar(int n) { return n % 2 == 0; }

float multInt1(int n, float m) {
    float r = 0;
    assert(n > 0);
    int i;
    for (i = 0; i < n; i++) {
        r += m;
    }
    return r;
}

float multInt2(int n, float m) {
    float r = 0;
    int c = 0;
    assert(n > 0);

    while (n >= 1) {
        if (!ePar(n)) {
            r += m;
            c++;
        }
        n /= 2;
        m *= 2;
        c += 2;
    }

    printf("Numero de operacoes: %d\n", c);

    return r;
}

void cmpMultInts(int n, float m) {
    float r1 = multInt1(n, m);
    printf("Numero de operacoes: %d\n", n);
    printf("multInt1(%d, %.2f) = %.2f\n", n, m, r1);

    float r2 = multInt2(n, m);
    printf("multInt2(%d, %.2f) = %.2f\n", n, m, r2);

    printf("\n\n");
}

int mdc1_a(int a, int b) {
    int r = 1;
    int menor = a < b ? a : b;
    int encontrou = 0;

    while (menor > 0 && encontrou == 0) {
        if (a % menor == 0 && b % menor == 0) {
            r = menor;
            encontrou = 1;
        }
    }

    return r;
}

int mdc1_b(int a, int b) {
    int menor = a < b ? a : b;

    while (menor > 0) {
        if (a % menor == 0 && b % menor == 0) {
            return menor;
        }
    }

    return 1;
}

int mdc2_rec(int a, int b) {
    if (a == b)
        return a;

    if (a > b) {
        return mdc2_rec(a - b, b);
    } else {
        return mdc2_rec(a, b - a);
    }
}

int mdc2_it(int a, int b) {
    while (a != b) {
        if (a > b) {
            a -= b;
        } else {
            b -= a;
        }
    }

    return a;
}

int mdc2_it2(int a, int b) {
    while (a != 0 && b != 0) {
        if (a > b) {
            a %= b;
        } else if (a < b) {
            b %= a;
        } else {
            return a;
        }
    }

    if (a == 0)
        return b;
    return a;
}

int main() {
    assert(multInt1(2, 3) == 6);
    assert(multInt2(2, 3) == 6);

    assert(multInt1(81, 423) == 34263);
    assert(multInt2(81, 423) == 34263);

    assert(multInt1(10, 3) == 30);
    assert(multInt2(10, 3) == 30);
    /*cmpMultInts(81, 423);*/
    /*cmpMultInts(2, 3);*/
    /*cmpMultInts(256, 89);*/
    /*int i;*/

    /*for (i = 1; i < 25; i++) cmpMultInts(i, 423);*/

    assert(mdc2_rec(12, 8) == 4);
    assert(mdc2_it(12, 8) == 4);
    assert(mdc2_it2(12, 8) == 4);

    assert(mdc2_rec(8, 12) == 4);
    assert(mdc2_it(8, 12) == 4);

    assert(mdc2_it2(8, 12) == 4);

    assert(mdc2_rec(126, 45) == 9);
    assert(mdc2_it(126, 45) == 9);
    assert(mdc2_it2(126, 45) == 9);

    assert(mdc2_rec(45, 126) == 9);
    assert(mdc2_it(45, 126) == 9);
    assert(mdc2_it2(45, 126) == 9);

    return 0;
}

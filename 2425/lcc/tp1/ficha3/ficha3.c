#include <assert.h>
#include <stdio.h>

void swapM(int *x, int *y) {
    int m = *x;
    (*x) = (*y);
    (*y) = m;
}

void swap_1(int v[], int i, int j) {
    int m = v[i];
    v[i] = v[j];
    v[j] = m;
}

void swap_2(int v[], int i, int j) {
    int m = *(v + i);
    *(v + i) = *(v + j);
    *(v + j) = m;
}

void swap_3(int v[], int i, int j) { swapM(v + i, v + j); }

int maximum(int v[], int N, int *m) {
    if (N < 1) {
        return 1;
    }

    int i;

    *m = v[0];
    for (i = 1; i < N; i++) {
        if (v[i] > *m) {
            *m = v[i];
        }
    }

    return 0;
}

void quadrados_1(int q[], int N) {
    q[0] = 0;
    int i;
    for (i = 1; i < N; i++) {
        /*(a+1)^2 = (a^2) + 2*a +1*/
        q[i] = q[i - 1] + 2 * (i - 1) + 1;
    }
}

void quadrados(int q[], int N) {
    int i;
    for (i = 0, q[0] = 0; i <= N; i++) {
        /*(a+1)^2 = (a^2) + 2*a +1*/
        q[i + 1] = q[i] + 2 * i + 1;
    }
}

void pascal(int v[], int N) {
    if (N == 0) {
        v[0] = 1;
    } else {

        int i;
        v[0] = 1;
        v[N] = 1;
        int ant[N];

        pascal(ant, N - 1);

        for (i = 1; i <= N - 1; i++) {
            v[i] = ant[i - 1] + ant[i];
        }
    }
}

int main() {
    int x = 3, y = 5;
    swapM(&x, &y);
    assert(x == 5);
    assert(y == 3);
    swapM(&x, &y);
    assert(x == 3);
    assert(y == 5);

    int N = 10;
    int q[N];
    quadrados(q, N);

    assert(q[1] == 1);
    assert(q[2] == 4);

    int i;
    for (i = 0; i < N; i++) {
        assert(q[i] == i ^ 2);
    }

    int v[4];
    pascal(v, 3);

    for (i = 0; i < 4; i++)
        printf("%d ", v[i]);
    printf("\n");

    int t[6];
    pascal(t, 5);

    for (i = 0; i < 6; i++)
        printf("%d ", t[i]);
    printf("\n");

    /*int m = x;*/
    /*x = y;*/
    /*y = m;*/
    /*swapM(&x, &y);*/
    /*printf("x = %d; y = %d;\n", x, y);*/
    /*swap(v, 0, 2);*/
    /*printf("%d %d\n", v[0], v[2]);*/

    return 0;
}

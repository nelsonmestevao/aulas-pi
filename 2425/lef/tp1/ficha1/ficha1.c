#include <stdio.h>

/*
Compilar no terminal com:

gcc ficha1.c -o programa

./programa

Numa só linha:

gcc ficha1.c -o programa && ./programa

Ou:

gcc ficha1.c -o programa; ./programa
*/

void linha(int n) {
    int i;
    for (i = 0; i < n; i++) {
        putchar('#');
    }
    putchar('\n');
}

void quadrado(int n) {
    int i;
    for (i = 0; i < n; i++) {
        linha(n);
    }
}

void xadrez(int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if ((j + i) % 2 == 0) {
                putchar('_');
            } else {
                putchar('#');
            }
        }
        putchar('\n');
    }
}

void trianguloH(int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 1; j <= 2 * n - 1; j++) {
            if ((j < (n - i)) || (j > (n + i)))
                putchar(' ');
            else
                putchar('#');
        }
        printf("\n");
    }
}

void trianguloH2(int n) {
    int i, j;

    for (i = 1; i <= n; i++) {
        for (j = 0; j < n - i; j++) {
            putchar(' ');
        }

        for (j = 0; j < 2 * i - 1; j++)
            putchar('#');
        putchar('\n');
    }
}

void trianguloV(int n) {
    int i, j;
    for (i = 1; i <= n; i++) {
        for (j = 0; j < i; j++)
            printf("#");
        putchar('\n');
    }
    for (i = n - 1; i > 0; i--) {
        for (j = 0; j < i; j++)
            printf("#");
        putchar('\n');
    }
}

int circulo(int raio) {
    printf("Circulo %d ainda não está feito...\n", raio);
    return 0;
}

int main() {
    // quadrado(5);
    // xadrez(5);
    // printf("Triangulo Vertical: 5\n");
    // trianguloV(5);
    // printf("Triangulo Vertical: 4\n");
    // trianguloV(4);

    printf("Triangulo Horizontal: 5\n");
    trianguloH2(5);
    printf("Triangulo Horizontal: 4\n");
    trianguloH2(4);
    // printf("Triangulo Horizontal: 4\n");
    // trianguloV(4);
    //
    // trianguloH(5);
    // trianguloV(5);
    // printf("\nForam usados %d caracteres para fazer o circulo\n",
    // circulo(5));
    return 0;
}

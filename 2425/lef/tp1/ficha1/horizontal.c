#include <stdio.h>

void triangulo(int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 1; j <= 2 * n - 1; j++) {
            if ((j < (n - i)) || (j > (n + i)))
                putchar(' ');
            else
                putchar('#');
        }
        printf("\n");
    }
}

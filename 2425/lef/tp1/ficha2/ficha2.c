#include <assert.h>
#include <stdio.h>

float multInt1(int n, float m) {
    int i;
    float soma = 0;
    int c = 0;
    for (i = 0; i < n; i++) {
        soma += m;
        c++;
    }
    printf("Total Operações: %i\n", c);
    return soma;
}

float multInt2(int n, float m) {
    float r = 0;
    int c = 0;

    while (n > 0) {
        if (n % 2 == 1) {
            r += m;
            c++;
        }

        n /= 2;
        m *= 2;
        c += 2;
    }

    printf("Total Operações: %i\n", c);

    return r;
}

int min(int a, int b) {
    if (a > b)
        return b;

    return a;
}

int mdc1(int a, int b) {
    int m = min(a, b);

    while (m > 0) {
        if (a % m == 0 && b % m == 0)
            return m;
        m--;
    }

    return -1;
}

int mdc2_rec(int a, int b) {
    if (a == b)
        return a;

    if (a > b)
        return mdc2_rec(a - b, b);

    return mdc2_rec(a, b - a);
}

int mdc2(int a, int b) {
    int c = 0;
    while (a != b) {
        if (a > b) {
            c++;
            a -= b;
        } else {
            c++;
            b -= a;
        }
    }

    printf("Total Operacoes: %d\n", c);
    return a;
}

int mdc2_otimizacao(int a, int b) {
    int c = 0;
    while (a > 0 && b > 0) {
        if (a > b) {
            a %= b;
            c++;
        } else if (a < b) {
            b %= a;
            c++;
        } else {
            return a;
        }
    }

    printf("Total Operacoes: %d\n", c);
    if (a == 0)
        return b;
    return a;
}

void cmpMI(int n, float m) {
    float r1 = multInt1(n, m);
    printf("multInt1(%i, %.2f) = %.2f\n", n, m, r1);

    float r2 = multInt2(n, m);
    printf("multInt2(%i, %.2f) = %.2f\n", n, m, r2);
}

int main() {
    printf("Ficha 2\n");
    printf("==================\n");

    /*printf("Exercicio 1 & 2\n");*/

    /*int i;*/
    /*for(i = 1; i < 50; i++) {*/
    /*    cmpMI(i, 42);*/
    /*}*/
    /*cmpMI(3, 6);*/
    /*cmpMI(6, 6);*/
    /*cmpMI(81, 423);*/
    /*cmpMI(423, 81);*/

    printf("Exercicio 4\n");

    printf("\n---\nEx. 4\n---\n");

    int a = 126;
    int b = 45;
    printf("MDC(%i, %i) = %i\n", a, b, mdc2(a, b));
    /*printf("MDC(%i, %i) = %i\n", b, a, mdc2(b, a));*/

    /*a = 8;*/
    /*b = 12;*/
    /*printf("MDC(%i, %i) = %i\n", b, a, mdc2(b, a));*/
    /*printf("MDC(%i, %i) = %i\n", a, b, mdc2(a, b));*/
    /**/
    /*a = 8;*/
    /*b = 8;*/
    /*printf("MDC(%i, %i) = %i\n", a, b, mdc2(a, b));*/

    printf("\n---\nEx. 4 (recursivo)\n---\n");

    a = 126;
    b = 45;
    printf("MDC(%i, %i) = %i\n", a, b, mdc2_rec(a, b));
    /*printf("MDC(%i, %i) = %i\n", b, a, mdc2_rec(b, a));*/

    /*a = 8;*/
    /*b = 12;*/
    /*printf("MDC(%i, %i) = %i\n", b, a, mdc2_rec(b, a));*/
    /*printf("MDC(%i, %i) = %i\n", a, b, mdc2_rec(a, b));*/
    /**/
    /*a = 8;*/
    /*b = 8;*/
    /*printf("MDC(%i, %i) = %i\n", a, b, mdc2_rec(a, b));*/

    printf("\n---\nEx. 5\n---\n");

    a = 126;
    b = 45;
    printf("MDC(%i, %i) = %i\n", a, b, mdc2_otimizacao(a, b));
    /*printf("MDC(%i, %i) = %i\n", b, a, mdc2_otimizacao(b, a));*/

    /*a = 8;*/
    /*b = 12;*/
    /*printf("MDC(%i, %i) = %i\n", b, a, mdc2_otimizacao(b, a));*/
    /*printf("MDC(%i, %i) = %i\n", a, b, mdc2_otimizacao(a, b));*/
    /**/
    /*a = 8;*/
    /*b = 8;*/
    /*printf("MDC(%i, %i) = %i\n", a, b, mdc2_otimizacao(a, b));*/

    return 0;
}

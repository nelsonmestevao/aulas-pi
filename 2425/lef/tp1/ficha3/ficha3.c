#include <stdio.h>
#include <assert.h>

void swapM(int *a, int *b) {
    int m = *a;
    (*a) = (*b);
    (*b) = m;
}

void swap(int v[], int i, int j) {
    int m = v[i];
    v[i] = v[j];
    v[j] = m;
}

void swap2(int v[], int i, int j) {
    int m = *(v + i);
    *(v + i) = *(v + j);
    *(v + j) = m;
}
void swap3(int v[], int i, int j) {
    swapM(v+i,v+j);
}

void printArray(int v[], int N) {
    int i;
    for(i = 0; i < N; i++)
        printf("%d  ", v[i]);
    printf("\n");
}

int maximum (int v[], int N, int *p) {
    if (N <= 0) return 1;

    *p = v[0];
    int i;
    for (i = 1; i < N; i++) {
        if (v[i] > (*p)) *p = v[i];
    }
    return 0;
}

int main() {
    int m;
    int a[4] = {3, 22, 50, 12};

    int r = maximum(a, 4, &m);

    if (r == 0) {
        printf("maior = %d\n", m);
    }

    int x = 3, y = 5;

    swapM(&x, &y);
    printf("x = %d; y = %d;\n", x, y);
    assert(x == 5);
    assert(y == 3);

    int v[4] = {23,89,71,42};

    int i = 1, j = 3;

    swap(v, i, j);

    printArray(v, 4);

    i = 2; j = 1;

    swap2(v, i, j);

    printArray(v, 4);

    i = 3; j = 2;

    swap3(v, i, j);

    printArray(v, 4);

    return 0;
}

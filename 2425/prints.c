#include <stddef.h>
#include <stdio.h>

// clang-format off
/**
## Variables Formatting

| Format | Data Type          | Description                                  | Example Output   |
|--------|--------------------|---------------------------------------------|-------------------|
| `%d`   | `int` (signed)     | Decimal (base 10)                           | `42`, `-99`       |
| `%i`   | `int` (signed)     | Decimal (same as `%d`, except in `scanf`)   | `42`, `-99`       |
| `%u`   | `unsigned int`     | Unsigned decimal                            | `42`, `99`        |
| `%o`   | `unsigned int`     | Octal (base 8)                              | `52` (for `42`)   |
| `%x`   | `unsigned int`     | Hexadecimal (lowercase)                     | `2a` (for `42`)   |
| `%X`   | `unsigned int`     | Hexadecimal (uppercase)                     | `2A` (for `42`)   |
|--------|--------------------|---------------------------------------------|-------------------|
| `%f`   | `float`, `double`  | Decimal notation (fixed-point)              | `3.141593`        |
| `%e`   | `float`, `double`  | Scientific notation (lowercase)             | `3.141593e+00`    |
| `%E`   | `float`, `double`  | Scientific notation (uppercase)             | `3.141593E+00`    |
| `%g`   | `float`, `double`  | Uses `%f` or `%e` (whichever is shorter)    | `3.14159`         |
| `%G`   | `float`, `double`  | Uses `%f` or `%E` (whichever is shorter)    | `3.14159`         |
|--------|--------------------|---------------------------------------------|-------------------|
| `%c`   | `char`             | Print a single character                    | `A`               |
| `%s`   | `char*`            | Print a string                              | `"Hello"`         |
|--------|--------------------|---------------------------------------------|-------------------|
| `%p`   | `void*`            | Print a pointer address                     | `0x7ffde4388a0c`  |
|--------|--------------------|---------------------------------------------|-------------------|
| `%zu`  | `size_t`           | Print `size_t` as unsigned decimal          | `8`               |
| `%zd`  | `ssize_t`          | Print `ssize_t` as signed decimal           | `-1`              |
| `%zx`  | `size_t`           | Print `size_t` as hexadecimal               | `ff`              |
| `%zo`  | `size_t`           | Print `size_t` as octal                     | `77`              |
|--------|--------------------|---------------------------------------------|-------------------|

## Formatting Flags

| Flag  | Description                      | Example (`printf("%+d", 42);`) |
|-------|----------------------------------|--------------------------------|
| `+`   | Print sign for positive numbers  | `+42`                          |
| `-`   | Left-align the output            | `42    `                       |
| `0`   | Pad numbers with leading zeros   | `0042`                         |
| `#`   | Use 0x for hex, 0 for octal      | `0x2a` (for `42`)              |
| `.`   | Set precision for floats/strings | `%.2f` → `3.14`                |
| `%%`  | Print a literal percent sign `%` | `%`                            |
*/
// clang-format on

int main() {
    // Integer formats
    int i = 42;
    unsigned int u = 99;
    printf("%%d: %d\n", i);
    printf("%%i: %i\n", i);
    printf("%%u: %u\n", u);
    printf("%%o: %o\n", u);
    printf("%%x: %x\n", u);
    printf("%%X: %X\n", u);

    // Floating-point formats
    // "Sim, é útil e fácil memorizar um número grato aos sábios"
    double pi = 3.1415926536;
    printf("%%f: %f\n", pi);
    printf("%%e: %e\n", pi);
    printf("%%E: %E\n", pi);
    printf("%%g: %g\n", pi);
    printf("%%G: %G\n", pi);

    // Character and string formats
    char c = 'A';
    char str[] = "Hello";
    printf("%%c: %c\n", c);
    printf("%%s: %s\n", str);

    // Pointer format
    void *ptr = &i;
    printf("%%p: %p\n", ptr);

    // size_t and platform-dependent formats
    size_t sz = sizeof(int);
    printf("%%zu: %zu\n", sz);
    printf("%%zd: %zd\n", (ssize_t)-1);
    printf("%%zx: %zx\n", sz);
    printf("%%zo: %zo\n", sz);

    // Formatting flags
    printf("%%+d: %+d\n", i);
    printf("%%-5d: %-5dEND\n", i);
    printf("%%05d: %05d\n", i);
    printf("%%#x: %#x\n", i);
    printf("%%.2f: %.2f\n", pi);

    return 0;
}

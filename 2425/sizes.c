#include <stdio.h>

int main() {
    printf("\nUnsigned\n");

    printf("char: %zu bytes\n", sizeof(char));
    printf("short: %zu bytes\n", sizeof(short));
    printf("int: %zu bytes\n", sizeof(int));
    printf("long: %zu bytes\n", sizeof(long));
    printf("float: %zu bytes\n", sizeof(float));
    printf("double: %zu bytes\n", sizeof(double));
    printf("long double: %zu bytes\n", sizeof(long double));

    printf("\nDecimal\n");

    printf("char: %zd bytes\n", sizeof(char));
    printf("short: %zd bytes\n", sizeof(short));
    printf("int: %zd bytes\n", sizeof(int));
    printf("long: %zd bytes\n", sizeof(long));
    printf("float: %zd bytes\n", sizeof(float));
    printf("double: %zd bytes\n", sizeof(double));
    printf("long double: %zd bytes\n", sizeof(long double));

    printf("\nHex\n");

    printf("char: %zx bytes\n", sizeof(char));
    printf("short: %zx bytes\n", sizeof(short));
    printf("int: %zx bytes\n", sizeof(int));
    printf("long: %zx bytes\n", sizeof(long));
    printf("float: %zx bytes\n", sizeof(float));
    printf("double: %zx bytes\n", sizeof(double));
    printf("long double: %zx bytes\n", sizeof(long double));

    printf("\nOctal\n");

    printf("char: %zo bytes\n", sizeof(char));
    printf("short: %zo bytes\n", sizeof(short));
    printf("int: %zo bytes\n", sizeof(int));
    printf("long: %zo bytes\n", sizeof(long));
    printf("float: %zo bytes\n", sizeof(float));
    printf("double: %zo bytes\n", sizeof(double));
    printf("long double: %zo bytes\n", sizeof(long double));

    return 0;
}

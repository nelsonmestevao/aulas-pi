#==============================================================================
SHELL   = sh
#------------------------------------------------------------------------------
NO_COLOR=\x1b[0m
OK_COLOR=\x1b[32m
ERROR_COLOR=\x1b[31m
WARN_COLOR=\x1b[33m
OK_STRING=$(OK_COLOR)✓$(NO_COLOR)
ERROR_STRING=$(ERROR_COLOR)⨯$(NO_COLOR)
WARN_STRING=$(WARN_COLOR)problems$(NO_COLOR)
#==============================================================================


fmt:
	@echo -n "Running formatter... "
	@find . -type f -name "*.c" | xargs clang-format -i
	@echo -e "$(OK_STRING)"

linter:
	@echo -n "Running linter... "
	@find . -type f -name "*.c" | xargs clang-tidy --fix-errors
	@echo -e "$(OK_STRING)"
